#include "head.h"
#include "cudamatrix.h"
#include "fileops.h"
#include "DataType.h"
#include <omp.h>

class RNNLM
{
protected:
    string inmodelfile, outmodelfile, trainfile, validfile,
           testfile, inputwlist, outputwlist, nglmstfile,
           sampletextfile, uglmfile, feafile;
    vector<int> &layersizes;
    map<string, int> inputmap, outputmap;
    vector<string>  inputvec, outputvec, ooswordsvec;
    vector<float>   ooswordsprob;
    vector<matrix *> layers, neu_ac, neu_er, gradlayers, accgradlayers;
    matrix *layer0_hist, *neu0_ac_hist, *neu0_er_hist, *layer0_fea, *feamatrix,
           *neu0_ac_fea, *layerN_class, *neuN_ac_class, *neuN_er_class,
           *bptt_layer0_hist, *lognorms, *gradlayer0_hist, *accgradlayer0_hist;
    matrix **bptt_hiddenac, **bptt_hiddener;
    set<int> noupdateset;
    float logp, llogp, gradient_cutoff, alpha, min_improvement, nwordspersec,
          lognorm, vrpenalty, lognorm_mean, lognorm_var, log_num_noise,
          lognormconst, lambda, version;
    double trnlognorm_mean;
    int rand_seed, deviceid, minibatch, debug, iter, lrtunemode, traincritmode,
        inputlayersize, outputlayersize, num_layer, wordcn, trainwordcnt,
         validwordcnt, independent, inStartindex, inOOSindex, cachesize,
        outEndindex, outOOSindex, k, bptt, bptt_delay, counter,
        fullvocsize, N, prevword, curword, num_oosword, nsample, nthread,
        num_fea, dim_fea, nclass;
    int *dev_bptt_history, *host_bptt_history, *host_prevwords, *host_curwords,
        *dev_prevwords, *dev_curwords, *ncesample, *dev_ncesample, *feaindices,
        *mbfeaindices, *word2class, *classinfo, *dev_classinfo,
        *dev_curclass, *host_curclass;
    bool alpha_divide, binformat, flag_usegpu;
    float *unigram, *logunigram, *accprob,  // won't allocate memory
          *logwordnoise, *dev_logwordnoise, *resetAc; // allocate memory
    auto_timer timer_sampler, timer_forward, timer_output, timer_backprop, timer_hidden;

    bool one_iter;

public:
    RNNLM(string inmodelfile_1, string outmodelfile_1, string inputwlist_1, string outputwlist_1, vector<int> &layersizes_1, int deviceid_1, int bptt_1, int bptt_delay_1, int mbsize, int rand_seed_1, bool binformat_1);
    RNNLM(string inmodelfile_1, string inputwlist_1, string outputwlist_1, vector<int> &lsizes, int fvocsize, bool bformat, int mbsize, int debuglevel, int deviceid_1, bool flag_gpu=true);

    ~RNNLM();

    bool train (string trainfile, string validfile, float learnrate, int deviceid, int csize, int fullvocsize, int independent, int debug);

    bool calppl (string testfilename, float lambda, string nglmfile, int fullvocsize);

    bool calnbest (string testfilename, float lambda, string nglmfile, int fullvocsize);


    bool sample (string sampletextfile, string unigramfile, int nsample);

    void cuForward (int evalmode=0);
    void cuBpttAndUpdate();
    void cuBpttOperation();
    void CollectEntropy(int evalmode=0);
    void InitVariables ();
    void LoadRNNLM(string modelname);
    void LoadBinaryRNNLM(string modelname);
    void LoadTextRNNLM(string modelname);
    void WriteRNNLM(string modelname);
    void WriteBinaryRNNLM(string modelname);
    void WriteTextRNNLM(string modelname);
    void WriteRNNLM_oldformat(string modelname);
    void cpyRnnLMfromGPU ();
    void cpyRnnLMtoGPU ();
    void ReadWordlist (string inputlist, string outputlist);
    void WriteWordlist (string inputlist, string outputlist);
    void preprocessforward ();
    void printTrainInfo ();
    void printPPLInfo ();
    void printSampleInfo ();

    void cucopyHiddenLayerToInput();
    void init();

    void SelectDevice ();
    void setLRtunemode (string lrtune);
    void setTrainCritmode (string traincrit);
    void setNumncesample (int n)    {k = n;}
    void setMinimprovement (float v) {min_improvement = v;}
    void setLognormConst (float v)   {lognormconst = v;}
    void setNthread (int n)         {nthread = n;}
    void setVRpenalty (float v)     {vrpenalty = v;}
    void setFullVocsize (int n)     {fullvocsize = n;}
    void setIndependentmode (int v);
    void setNclass (int n);
    void prepareNCEtrain ();
    int  genOnesample ();
    void genNCESample ();
    void forward_NCE ();
    void calerr_NCE();
    void bpttAndupdate_NCE ();
    void updateInputWordWgts(int *inwords);
    void forwardInputWordWgts ();
    void copyRecurrentAc ();
    void ResetRechist();
    float forword (int prevword, int curword);
    void ReadUnigramFile (string unigramfile);
    void matrixXvector (float *ac0, float *wgt1, float *ac1, int nrow, int ncol);
    void allocMem (vector<int> &layersizes);
    void allocBpttMem ();
    void allocWordMem ();
    void randomWeights ();

    // functions for using additional feature file in input layer
    void ReadFeaFile(string filestr);
    void fillInputFeature (matrix *neu0_ac_fea, matrix *feamatrix, int *feaindices, int *mbfeaindices);
    void setFeafile (string str)  {feafile = str;}

    // functions for class based output layer
    void getClassLabels ();
    void clusterWord2Class (string trainfile, map<string, int> &wordmap, vector<string> &wordvec);

    void setOneIter (bool x) {one_iter=x;}


    };


