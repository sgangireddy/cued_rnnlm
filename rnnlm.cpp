#include "rnnlm.h"

void RNNLM::init()
{
    gradient_cutoff = 0;            // default no gradient cutoff
    llogp           = -1e8;
    min_improvement = 1.001;
    lognormconst    = 0;
    lrtunemode      = 0;                 // default use newbob
    alpha_divide    = false;
    alpha           = 0.8;
    lambda          = 0.5;
    version         = 0.1;
    iter            = 0;
    num_layer       = 0;
    wordcn          = 0;
    trainwordcnt    = 0;
    validwordcnt    = 0;
    counter         = 0;
    N               = 0;
    num_oosword     = 0;
    num_fea         = 0;
    dim_fea         = 0;
    bptt_layer0_hist= NULL;
    bptt_hiddener   = NULL;
    bptt_hiddenac   = NULL;
    dev_bptt_history= NULL;
    host_bptt_history = NULL;
    ncesample       = NULL;
    dev_ncesample   = NULL;
    logwordnoise    = NULL;
    dev_logwordnoise= NULL;
    gradlayer0_hist = NULL;
    accgradlayer0_hist = NULL;
    resetAc         = NULL;
    host_prevwords  = NULL;
    host_curwords   = NULL;
    dev_prevwords   = NULL;
    dev_curwords    = NULL;
    logwordnoise    = NULL;
    dev_logwordnoise = NULL;
    neu0_er_hist    = NULL;
    lognorms        = NULL;
    flag_usegpu     = true;
    layer0_fea      = NULL;
    neu0_ac_fea     = NULL;
    feamatrix       = NULL;
    feaindices      = NULL;
    mbfeaindices    = NULL;
    word2class      = NULL;
    classinfo       = NULL;
    dev_classinfo   = NULL;
    layerN_class    = NULL;
    neuN_ac_class   = NULL;
    neuN_er_class   = NULL;
    host_curclass   = NULL;
    dev_curclass    = NULL;
    nclass          = 0;
}

RNNLM::~RNNLM()
{
    int i;
    for (i=0; i<num_layer; i++)
    {
        delete layers[i];
    }
    if (dev_prevwords)      cufree(dev_prevwords);
    if (dev_curwords)       cufree(dev_curwords);
    if (ncesample)          free(ncesample);
    if (dev_ncesample)      cufree(dev_ncesample);
    if (host_prevwords)     free(host_prevwords);
    if (host_curwords)      free(host_curwords);
    if (logwordnoise)       free (logwordnoise);
    if (dev_logwordnoise)   cufree (dev_logwordnoise);
    if (neu0_ac_hist)       delete neu0_ac_hist;
    if (neu0_er_hist)       delete neu0_er_hist;
    if (layer0_hist)        delete layer0_hist;
    if (bptt_layer0_hist)   delete bptt_layer0_hist;
    if (dev_bptt_history)   cufree(dev_bptt_history);
    if (host_bptt_history)  free (host_bptt_history);
    if (lognorms)           delete lognorms;
    if (bptt_hiddenac)
    {
        for (i=0; i<bptt+bptt_delay+1; i++)
        {
            delete bptt_hiddenac[i];
            delete bptt_hiddener[i];
        }
        delete [] bptt_hiddener;
        delete [] bptt_hiddenac;
    }
    return ;
    for (i=0; i<num_layer; i++)
    {
        delete layers[i];
        delete neu_ac[i];
        delete neu_er[i];
    }
    if (lrtunemode != 0)
    {
        for (i=0; i<num_layer; i++)
        {
            delete gradlayers[i];
            delete accgradlayers[i];
        }
        delete gradlayer0_hist;
        delete accgradlayer0_hist;
    }
    delete neu_ac[num_layer];
    delete neu_er[num_layer];
    if (resetAc)    delete [] resetAc;
    if (layer0_fea) {delete layer0_fea; layer0_fea=NULL;}
    if (neu0_ac_fea) {delete neu0_ac_fea; neu0_ac_fea=NULL;}
    if (feamatrix)  {delete feamatrix; feamatrix=NULL;}
    if (mbfeaindices) {free(mbfeaindices); mbfeaindices=NULL;}
}


RNNLM::RNNLM(string inmodelfile_1, string inputwlist_1, string outputwlist_1, vector<int> &lsizes, int fvocsize, bool bformat, int mbsize, int debuglevel, int deviceid_1, bool flag_gpu /*=true*/):inmodelfile(inmodelfile_1), inputwlist(inputwlist_1), outputwlist(outputwlist_1), layersizes(lsizes), fullvocsize(fvocsize), binformat(bformat), minibatch(mbsize), debug(debuglevel), deviceid(deviceid_1)
{
    int i;
    init ();
    flag_usegpu = flag_gpu;
    if (flag_usegpu)
    {
        SelectDevice ();
    }
    else
    {
        minibatch = 1;
    }
    LoadRNNLM (inmodelfile);
    ReadWordlist (inputwlist, outputwlist);

    if (fullvocsize == 0)
    {
        fullvocsize = layersizes[0];
    }

    resetAc = new float[layersizes[1]];
    memcpy(resetAc, neu0_ac_hist->gethostdataptr(), sizeof(float)*layersizes[1]);
    if (flag_usegpu)
    {
        allocWordMem();
    }
}


RNNLM::RNNLM(string inmodelfile_1, string outmodelfile_1, string inputwlist_1, string outputwlist_1, vector<int> &layersizes_1, int deviceid_1, int bptt_1, int bptt_delay_1, int mbsize, int rand_seed_1, bool binformat_1) : inmodelfile(inmodelfile_1), outmodelfile(outmodelfile_1), inputwlist(inputwlist_1), outputwlist(outputwlist_1), layersizes(layersizes_1), deviceid(deviceid_1), bptt(bptt_1), bptt_delay(bptt_delay_1), minibatch(mbsize), rand_seed(rand_seed_1), binformat(binformat_1)
{
    int i;
    init ();
    // select proper gpu device
    SelectDevice ();
    if (isEmpty(inmodelfile))       // train from scratch
    {
        allocMem (layersizes);
        randomWeights ();
    }
    else
    {
        LoadRNNLM (inmodelfile);
    }
    ReadWordlist (inputwlist, outputwlist);
    allocBpttMem();
    allocWordMem();
}

// allocate memory for RNNLM model
void RNNLM::allocMem (vector<int> &layersizes)
{
    int i;
    num_layer = layersizes.size() - 1;
    if (num_layer < 2)
    {
        printf ("ERROR: the number of layers (%d) should be greater than 2\n", num_layer);
    }
    inputlayersize = layersizes[0] + layersizes[1];
    outputlayersize = layersizes[num_layer];
    layer0_hist = new matrix (layersizes[1], layersizes[1], flag_usegpu);
    neu0_ac_hist = new matrix (layersizes[1], minibatch, flag_usegpu);
    neu0_er_hist = new matrix (layersizes[1], minibatch, flag_usegpu);
    layers.resize(num_layer);
    neu_ac.resize(num_layer+1);
    neu_er.resize(num_layer+1);
    for (i=0; i<num_layer; i++)
    {
        int nrow = layersizes[i];
        int ncol = layersizes[i+1];
        layers[i] = new matrix (nrow, ncol, flag_usegpu);
    }
    for (i=0; i<layersizes.size(); i++)
    {
        int nrow = layersizes[i];
        neu_ac[i] = new matrix (nrow, minibatch, flag_usegpu);
        neu_er[i] = new matrix (nrow, minibatch, flag_usegpu);
    }
    if (dim_fea > 0)
    {
        layer0_fea = new matrix (dim_fea, layersizes[1], flag_usegpu);
        neu0_ac_fea = new matrix (dim_fea, minibatch);
        neu0_ac_fea->initmatrix();
    }
    if (nclass > 0)
    {
        layerN_class = new matrix (layersizes[num_layer-1], nclass);
        neuN_ac_class = new matrix (nclass, minibatch);
        neuN_er_class = new matrix (nclass, minibatch);
        neuN_ac_class->initmatrix ();
        neuN_er_class->initmatrix ();
    }
}

// allocate memory used for BPTT during training
void RNNLM::allocBpttMem ()
{
    int i;
    bptt_layer0_hist = new matrix (layersizes[1], layersizes[1]);
    bptt_layer0_hist->initmatrix(0);
    bptt_hiddenac = new matrix* [bptt+bptt_delay+1];
    bptt_hiddener = new matrix* [bptt+bptt_delay+1];
    for (i=0; i<bptt+bptt_delay+1; i++)
    {
        bptt_hiddenac[i] = new matrix (layersizes[1], minibatch);
        bptt_hiddener[i] = new matrix (layersizes[1], minibatch);
        bptt_hiddenac[i]->initmatrix(0);
        bptt_hiddener[i]->initmatrix(0);
    }
    dev_bptt_history = (int *)cucalloc ((bptt+bptt_delay+10) * minibatch * sizeof(int));
    cumemset (dev_bptt_history, -1, (bptt+bptt_delay+10) * minibatch * sizeof(int));
    host_bptt_history = (int *)calloc((bptt+bptt_delay+10)*minibatch, sizeof(int));
    memset (host_bptt_history, 0, (bptt+bptt_delay+10)*minibatch*sizeof(int));
}

// allocate memory used for input words, target words
void RNNLM::allocWordMem ()
{
    host_prevwords = (int *)calloc(minibatch, sizeof(int));
    host_curwords = (int *)calloc (minibatch, sizeof(int));
    dev_prevwords = (int *)cucalloc (minibatch * sizeof(int));
    dev_curwords = (int *)cucalloc (minibatch * sizeof(int));
    memset(host_prevwords, 0, sizeof(int)*minibatch);
    memset(host_curwords, 0, sizeof(int)*minibatch);
    cumemset(dev_prevwords, 0, sizeof(int)*minibatch);
    cumemset(dev_curwords, 0, sizeof(int)*minibatch);
    lognorms = new matrix (1, minibatch);
    lognorms->initmatrix(0);
}

// randomize initial RNNLM weight
void RNNLM::randomWeights ()
{
    int i;
    layer0_hist->initmatrix();
    layer0_hist->random(MINRANDINITVALUE, MAXRANDINITVALUE);
    for (i=0; i<num_layer; i++)
    {
        layers[i]->random(MINRANDINITVALUE, MAXRANDINITVALUE);
    }
}

void RNNLM::printTrainInfo ()
{
    printf ("train   txt:       %s\n", trainfile.c_str());
    printf ("valid   txt:       %s\n", validfile.c_str());
    printf ("input  list:       %s\n", inputwlist.c_str());
    printf ("output list:       %s\n", outputwlist.c_str());
    printf ("num   layer:       %d\n", num_layer);
    for (int i=0; i<=num_layer; i++)
    {
        printf ("#layer[%d]  :       %d\n", i, layersizes[i]);
    }
    if (dim_fea > 0)
    {
    printf ("feature file:      %s\n", feafile.c_str());
    printf ("dim  feature:      %d\n", dim_fea);
    }
    printf ("device   id:       %d\n", deviceid);
    printf ("nthread    :       %d\n", nthread);
    printf ("minibatch  :       %d\n", minibatch);
    printf ("learn  rate:       %f\n", alpha);
    printf ("cache  size:       %d\n", cachesize);
    printf ("bptt       :       %d\n", bptt);
    printf ("bptt  delay:       %d\n", bptt_delay);
    printf ("min  improv:       %f\n", min_improvement);
    printf ("independent:       %d\n", independent);
    printf ("debug level:       %d\n", debug);
    printf ("random seed:       %d\n", rand_seed);
    printf ("write model:       %s\n", outmodelfile.c_str());
    printf ("binary     :       %d\n", binformat);
    string str;
    if (lrtunemode == 0)        str = "newbob";
    else if(lrtunemode == 1)    str = "adagrad";
    else if(lrtunemode == 2)    str = "rmsprop";
    else                        str = "unknown";
    printf ("learn tune :       %s\n", str.c_str());
    if (traincritmode== 0)      str = "ce";
    else if (traincritmode==1)  str = "vr";
    else if (traincritmode==2)  str = "nce";
    else                        str = "unknown";
    printf ("train crit :       %s\n", str.c_str());
    printf ("vr penalty :       %f\n", vrpenalty);
    printf ("ncesample  :       %d\n", k);
}

void RNNLM::printPPLInfo ()
{
    printf ("model file :       %s\n", inmodelfile.c_str());
    printf ("input  list:       %s\n", inputwlist.c_str());
    printf ("output list:       %s\n", outputwlist.c_str());
    printf ("num   layer:       %d\n", num_layer);
    for (int i=0; i<=num_layer; i++)
    {
        printf ("#layer[%d]  :       %d\n", i, layersizes[i]);
    }
    printf ("independent:       %d\n", independent);
    printf ("test file  :       %s\n", testfile.c_str());
    printf ("nglm file  :       %s\n", nglmstfile.c_str());
    printf ("lambda (rnn):      %f\n", lambda);
    printf ("fullvocsize:       %d\n", fullvocsize);
    printf ("debug level:       %d\n", debug);
    printf ("nthread    :       %d\n", nthread);
}

void RNNLM::printSampleInfo ()
{
    printf ("model file :       %s\n", inmodelfile.c_str());
    printf ("input  list:       %s\n", inputwlist.c_str());
    printf ("output list:       %s\n", outputwlist.c_str());
    printf ("num   layer:       %d\n", num_layer);
    for (int i=0; i<=num_layer; i++)
    {
        printf ("#layer[%d]  :       %d\n", i, layersizes[i]);
    }
    printf ("independent:       %d\n", independent);
    printf ("device   id:       %d\n", deviceid);
    printf ("minibatch  :       %d\n", minibatch);
    printf ("text file  :       %s\n", sampletextfile.c_str());
    printf ("uglm file  :       %s\n", uglmfile.c_str());
    printf ("nsample    :       %d\n", nsample);
    printf ("fullvocsize:       %d\n", fullvocsize);
    printf ("debug level:       %d\n", debug);
    printf ("nthread    :       %d\n", nthread);
}

bool RNNLM::train (string trainfilename, string validfilename, float learnrate, int deviceid, int csize, int fullvocsize, int independent, int debuglevel)
{
    int randint, i, j;
    trainfile = trainfilename;
    validfile = validfilename;
    debug = debuglevel;
    alpha = learnrate / minibatch;
    cachesize = csize;
    int mbcntiter=0, mbcnt=0, localmbiter;

    if (debug > 0)
    {
        printTrainInfo();
        fflush(stdout);
    }
    // specify number of class in output layer
    // if word2class is not NULL, then it should be read from output list file
    if (nclass > 0)
    {
        clusterWord2Class (trainfile, outputmap, outputvec);
    }
    ReadFileBuf trainbuf(trainfile, inputmap, outputmap, minibatch, cachesize, num_fea);
    randint = trainbuf.getRandint();
    ReadFileBuf validbuf(validfile, inputmap, outputmap, minibatch, 0, num_fea);     // don't use cache on valid data
    if (dim_fea > 0)
    {
        mbfeaindices = (int *)malloc(sizeof(int)*minibatch);
    }
    if (cachesize == 0)     trainbuf.FillBuffer();
    validbuf.FillBuffer();
    trainwordcnt = trainbuf.getWordcnt();
    validwordcnt = validbuf.getWordcnt();
    if (traincritmode == 2)
    {
        unigram = trainbuf.getUnigram();
        logunigram = trainbuf.getLogUnigram();
        accprob = trainbuf.getAccprob ();
        prepareNCEtrain ();
        if (debug > 2)
        {
#ifdef NUM_THREAD
#pragma omp parallel for num_threads(NUM_THREAD)
#else
#pragma omp parallel for num_threads(nthread)
#endif
            for (int i = 0; i<10; i++)
            {
                printf( "Hello from thread %d, nthreads %d, max nthreads %d\n",
                       omp_get_thread_num(), omp_get_num_threads(), omp_get_max_threads());
            }
        }
    }
    if (debug > 0)
    {
        printf ("Read train txt file: %s, %d lines, %d words totally\n", trainfile.c_str(), trainbuf.getLinecnt(), trainwordcnt);
        printf ("Read valid txt file: %s, %d lines, %d words totally\n", validfile.c_str(), validbuf.getLinecnt(), validwordcnt);
        printf ("Start RNNLM training...initial learning rate per sample: %f\n", alpha);
    }
    while (1)
    {
        auto_timer timer;
        timer.start();
        InitVariables();
        if (traincritmode == 1 || traincritmode==2)
        {
            lognorm_mean = 0;
            lognorm_var  = 0;
            trnlognorm_mean = 0;
        }
        if (cachesize > 0)
        {
            trainbuf.Init();
        }
        // Train Stage
        mbcntiter = 0;
        mbcnt = trainbuf.getMBcnt();
#if 0
        if (lrtunemode != 0)
        {
            for (i=0; i<num_layer; i++)
            {
                gradlayers[i]->initmatrix();
                accgradlayers[i]->initmatrix();
            }
            gradlayer0_hist->initmatrix();
            accgradlayer0_hist->initmatrix();
        }
#endif
        if (dim_fea > 0)
        {
            feaindices = trainbuf.getfeaptr();
            for (i=0; i<minibatch; i++)     mbfeaindices[i] = i;
            fillInputFeature (neu0_ac_fea, feamatrix, feaindices, mbfeaindices);
        }
        while (1)
        {
            counter ++;
            if (mbcntiter >= mbcnt)          break;
            // if (noupdateset.size() > 0)     break;
            if (cachesize == 0)
            {
                localmbiter = mbcntiter;
            }
            else
            {
                if (mbcntiter%cachesize==0)
                {
                    trainbuf.FillBuffer();
                }
                localmbiter = mbcntiter % cachesize;
            }
            mbcntiter ++;
            // fetch data from buf to prevwords, curwords
            trainbuf.GetData(localmbiter, host_prevwords, host_curwords);
            if (nclass > 0)
            {
                getClassLabels();
            }
            for (set<int>::iterator it=noupdateset.begin(); it != noupdateset.end(); it++)
            {
                host_prevwords[*it] = 0;
                host_curwords[*it] = 0;
            }
            cucpytoGpu(dev_prevwords, host_prevwords, sizeof(int)*minibatch);
            cucpytoGpu(dev_curwords,  host_curwords,  sizeof(int)*minibatch);
#ifdef DEBUG
            timer_sampler.start();
#endif
            if (traincritmode == 2)         // NCE training
            {
                genNCESample ();
            }
#ifdef DEBUG
            timer_sampler.add();
#endif

            // RNNLM forward
            cuForward (0);

            // RNNLM BPPT operation, e.g.  save BPTT history
            cuBpttOperation ();

            // RNNLM back propogation through time (BPTT) and update parameter
            cuBpttAndUpdate ();

            // forward recurrent neuron
            cucopyHiddenLayerToInput();

            // collect entropy
            CollectEntropy (0);

            if (debug > 1 && counter % 100 == 0)
            {
                GPUsynchronizewithCPU();
                nwordspersec = wordcn / (timer.stop());
                printf("%cIter %3d\tAlpha: %.8f\t Train Entropy: %.4f\t Progress: %.2f%%\t Words/sec: %.3f", 13, iter, alpha, -logp/log10(2)/wordcn, wordcn*100.0/trainwordcnt, nwordspersec);
            }
        }
        printf ("%cIter %3d\tAlpha: %.8f\t Train Entropy: %.4f\t ", 13, iter, alpha, -logp/log10(2)/wordcn);
#ifdef DEBUG
        printf ("\nsample: %.4f, forward: %.4f, output: %.4f, backprop: %.4f\thidden: %.4f\n", timer_sampler.getacctime(), timer_forward.getacctime(), timer_output.getacctime(), timer_backprop.getacctime(), timer_hidden.getacctime());
        exit (0);
#endif
        nwordspersec = wordcn / (timer.stop());
        if (traincritmode == 1)      trnlognorm_mean = trnlognorm_mean/wordcn;
        if (traincritmode == 2)
        {
            // copy the weights to GPU for valid
            layers[num_layer-1]->assign();
            // trnlognorm_mean = CONSTLOGNORM;
        }
        fflush(stdout);

        // Valid Stage
        InitVariables ();
        mbcntiter = 0;
        mbcnt = validbuf.getMBcnt();
        if (dim_fea > 0)
        {
            feaindices = validbuf.getfeaptr();
            for (i=0; i<minibatch; i++)     mbfeaindices[i] = i;
            fillInputFeature (neu0_ac_fea, feamatrix, feaindices, mbfeaindices);
        }
        while (1)
        {
            if (mbcntiter >= mbcnt)          break;
            validbuf.GetData(mbcntiter, host_prevwords, host_curwords);
            if (nclass > 0)
            {
                getClassLabels();
            }
            // trainbuf.GetData(mbcntiter, host_prevwords, host_curwords);
            mbcntiter ++;
            for (set<int>::iterator it=noupdateset.begin(); it != noupdateset.end(); it++)
            {
                host_prevwords[*it] = 0;
                host_curwords[*it] = 0;
            }
            cucpytoGpu(dev_prevwords, host_prevwords, sizeof(int)*minibatch);
            cucpytoGpu(dev_curwords,  host_curwords,  sizeof(int)*minibatch);

            // RNNLM forward
            cuForward (1);
            cucopyHiddenLayerToInput();

            // collect entropy
            CollectEntropy (1);
        }

        // Print Statistics
        timer.add();
        printf ("Valid Entropy: %.4f\tPPL: %.3f  Words/sec: %.3f\tTime: %.2fs\n", -logp/log10(2)/wordcn, exp10(-logp/wordcn), nwordspersec, timer.getacctime());
        if (traincritmode == 1 || traincritmode==2)         // variance regularization
        {
            lognorm_var  = (lognorm_var - lognorm_mean*lognorm_mean/wordcn)/(wordcn-1);
            lognorm_mean = lognorm_mean/wordcn;
            printf ("Mean: %.3f Var: %.3f, TrnLogNorm: %.3f\n", lognorm_mean, lognorm_var,  trnlognorm_mean);
            trnlognorm_mean = 0;
            lognorm_mean = 0;
            lognorm_var = 0;
        }

        fflush(stdout); //flush the printf outputs to standard output terminal
        if (one_iter)
        {
        	printf("the model is trained for one iteraiton and saving the model..\n");
            break;

        }
        // Learn rate tune
        if (lrtunemode == 0)        // newbob
        {
            if (logp*min_improvement < llogp)
            {
                if (alpha_divide)   {break;}
                else                {alpha_divide = true;}
                // alpha_divide = true;
            }
            if (alpha_divide)   alpha /= 2;
        }
        else if (lrtunemode == 1)   // adagrad
        {
            alpha = alpha;
            if (logp*min_improvement < llogp)
            {
                break;
            }
        }
        else if (lrtunemode == 2)   // rmsprop
        {
            alpha = alpha*0.95;
            if (logp*min_improvement < llogp)
            {
                break;
            }
        }
        else
        {
            printf ("ERROR: unrecognized learning rate tune algorithm!\n");
        }

        char filename[1024];
        llogp = logp;
        iter ++;
        //if (debug > 2)
        //{
          if (binformat)
          {
               sprintf (filename, "%s.bin.iter%d", outmodelfile.c_str(), iter);
          }
          else
          {
               sprintf (filename, "%s.iter%d", outmodelfile.c_str(), iter);
          }
          WriteRNNLM (filename);
        //}
        fflush(stdout);
    }

    WriteRNNLM (outmodelfile);

    // delete the train and valid index file
    trainbuf.DeleteIndexfile();
    validbuf.DeleteIndexfile();

    return SUCCESS;
}


bool RNNLM::calppl (string testfilename, float intpltwght, string nglmfile, int fvocsize)
{
    int i, j, wordcn, nwordoov, cnt;
    vector<string> linevec;
    FILEPTR fileptr;
    float prob_rnn, prob_ng, prob_int, logp_rnn,
          logp_ng, logp_int, ppl_rnn, ppl_ng,
          ppl_int;
    bool flag_intplt = false, flag_oov = false;
    FILE *fptr=NULL, *fptr_nglm=NULL;
    auto_timer timer;
    timer.start();
    string word;
    testfile = testfilename;
    fullvocsize = fvocsize;
    nglmstfile = nglmfile;
    lambda = intpltwght;
    if (debug > 1)
    {
        printPPLInfo ();
    }

    if (!nglmfile.empty())
    {
        fptr_nglm = fopen (nglmfile.c_str(), "r");
        if (fptr_nglm == NULL)
        {
            printf ("ERROR: Failed to open ng stream file: %s\n", nglmfile.c_str());
            exit (0);
        }
        flag_intplt = true;
    }
    fileptr.open(testfile);

    wordcn = 0;
    nwordoov = 0;
    logp_int = 0;
    logp_rnn = 0;
    logp_ng = 0;
    for (int i=0; i<layersizes[1]; i++)     neu0_ac_hist->assignhostvalue(i, 0, RESETVALUE);
    if (debug > 1)
    {
        if (flag_intplt)
        {
            printf ("\nId\tP_rnn\t\tP_ng\t\tP_int\t\tWord\n");
        }
        else
        {
            printf ("\nId\tP_rnn\t\tWord\n");
        }
    }
    while (!fileptr.eof())
    {
        if (dim_fea > 0)
        {
            int feaid = fileptr.readint();
            assert (feaid >=0 && feaid < num_fea);
            float *feaptr = feamatrix->gethostdataptr(0, feaid);
            float *acptr  = neu0_ac_fea->gethostdataptr();
            memcpy(acptr, feaptr, sizeof(float)*dim_fea);
        }
        fileptr.readline(linevec, cnt);
        if (linevec.size() > 0)
        {
            if (linevec[cnt-1] != "</s>")
            {
                linevec.push_back("</s>");
                cnt ++;
            }
            assert (cnt == linevec.size());
            if (linevec[0] == "<s>")    i = 1;
            else                        i = 0;
            prevword = inStartindex;
            if (independent)
            {
                ResetRechist();
            }
            for (i; i<cnt; i++)
            {
                word = linevec[i];
                if (outputmap.find(word) == outputmap.end())
                {
                    curword = outOOSindex;
                }
                else
                {
                    curword = outputmap[word];
                }
                prob_rnn = forword (prevword, curword);
                if (curword == outOOSindex)     prob_rnn /= (fullvocsize-layersizes[num_layer]);
                copyRecurrentAc ();

                if (flag_intplt)
                {
                    if (fscanf (fptr_nglm, "%f\n", &prob_ng) != 1)
                    {
                        printf ("ERROR: Failed to read ngram prob from ng stream file!\n");
                        exit (0);
                    }
                    if (fabs(prob_ng) < 1e-9)   {flag_oov = true;}
                    else                        flag_oov = false;
                }
                prob_int = lambda*prob_rnn + (1-lambda)*prob_ng;
                if (inputmap.find(word) == inputmap.end())
                {
                    prevword = inOOSindex;
                }
                else
                {
                    prevword = inputmap[word];
                }
                if (!flag_oov)
                {
                    logp_rnn += log10(prob_rnn);
                    logp_ng  += log10(prob_ng);
                    logp_int += log10(prob_int);
                }
                else
                {
                    nwordoov ++;
                }
                wordcn ++;
                if (debug > 1)
                {
                    if (flag_intplt)
                        printf ("%d\t%.10f\t%.10f\t%.10f\t%s", curword, prob_rnn, prob_ng, prob_int, word.c_str());
                    else
                        printf ("%d\t%.10f\t%s", curword, prob_rnn, word.c_str());
                    if (curword == outOOSindex)
                    {
                        if (flag_oov)   printf ("<OOV>");
                        else            printf ("<OOS>");
                    }
                    printf ("\n");
                }
                if (debug > 2)
                {
                    if (wordcn % 10000 == 0)
                    {
                        float nwordspersec = wordcn / (timer.stop());
                        printf ("eval speed  %.4f Words/sec\n", nwordspersec);
                    }
                }
            }
        }
    }
    if (debug > 2)
    {
        float nwordspersec = wordcn / (timer.stop());
        printf ("eval speed  %.4f Words/sec\n", nwordspersec);
    }
    ppl_rnn = exp10(-logp_rnn/(wordcn-nwordoov));
    ppl_ng  = exp10(-logp_ng/(wordcn-nwordoov));
    ppl_int = exp10(-logp_int/(wordcn-nwordoov));
    if (flag_intplt)
    {
        printf ("Total word: %d\tOOV word: %d\n", wordcn, nwordoov);
        printf ("N-Gram log probability: %.3f\n", logp_ng);
        printf ("RNNLM  log probability: %.3f\n", logp_rnn);
        printf ("Intplt log probability: %.3f\n\n", logp_int);
        printf ("N-Gram PPL : %.3f\n", ppl_ng);
        printf ("RNNLM  PPL : %.3f\n", ppl_rnn);
        printf ("Intplt PPL : %.3f\n", ppl_int);
    }
    else
    {
        printf ("Total word: %d\tOOV word: %d\n", wordcn, nwordoov);
        printf ("Average logp: %f\n", logp_rnn/log10(2)/wordcn);
        printf ("RNNLM  log probability: %.3f\n", logp_rnn);
        printf ("RNNLM  PPL : %.3f\n", ppl_rnn);
    }
    fileptr.close();

    if (fptr_nglm)
    {
        fclose(fptr_nglm);
    }
    return SUCCESS;
}

bool RNNLM::calnbest (string testfilename, float intpltwght, string nglmfile, int fvocsize)
{
    int i, j, wordcn, cnt, nbestid, prevnbestid=-1, sentcnt=0;
    vector<string> linevec;
    FILEPTR fileptr;
    float prob_rnn, prob_ng, prob_int, logp_rnn,
          logp_ng, logp_int, ppl_rnn, ppl_ng,
          ppl_int, sentlogp;
    bool flag_intplt = false;
    FILE *fptr=NULL, *fptr_nglm=NULL;
    auto_timer timer;
    timer.start();
    string word;
    testfile = testfilename;
    fullvocsize = fvocsize;
    nglmstfile = nglmfile;
    lambda = intpltwght;
    if (debug > 1)
    {
        printPPLInfo ();
    }
    if (!nglmfile.empty())
    {
        fptr_nglm = fopen (nglmfile.c_str(), "r");
        if (fptr_nglm == NULL)
        {
            printf ("ERROR: Failed to open ng stream file: %s\n", nglmfile.c_str());
            exit (0);
        }
        flag_intplt = true;
    }
    fileptr.open(testfile);

    wordcn = 0;
    logp_int = 0;
    logp_rnn = 0;
    logp_ng = 0;
    for (int i=0; i<layersizes[1]; i++)
    {
        neu0_ac_hist->assignhostvalue(i, 0, RESETVALUE);
        resetAc[i] = RESETVALUE;
    }
    while (!fileptr.eof())
    {
        if (dim_fea > 0)
        {
            int feaid = fileptr.readint();
            assert (feaid >=0 && feaid < num_fea);
            float *feaptr = feamatrix->gethostdataptr(0, feaid);
            float *acptr  = neu0_ac_fea->gethostdataptr();
            memcpy(acptr, feaptr, sizeof(float)*dim_fea);
        }
        fileptr.readline(linevec, cnt);
        if (linevec.size() > 0)
        {
            if (linevec[cnt-1] != "</s>")
            {
                linevec.push_back("</s>");
                cnt ++;
            }
            assert (cnt == linevec.size());
            // the first two iterms for linevec are: <s> nbestid
            if (linevec[2] == "<s>")    i = 3;
            else                        i = 2;
            nbestid = string2int(linevec[1]);

            if (nbestid != prevnbestid)
            {
                memcpy(resetAc, neu0_ac_hist->gethostdataptr(), sizeof(float)*layersizes[1]);
            }
            else
            {
                memcpy(neu0_ac_hist->gethostdataptr(), resetAc, sizeof(float)*layersizes[1]);
            }
            sentlogp = 0;
            prevword = inStartindex;
            if (independent)
            {
                ResetRechist();
            }
            for (i; i<cnt; i++)
            {
                word = linevec[i];
                if (outputmap.find(word) == outputmap.end())
                {
                    curword = outOOSindex;
                }
                else
                {
                    curword = outputmap[word];
                }
                prob_rnn = forword (prevword, curword);
                if (curword == outOOSindex)     prob_rnn /= (fullvocsize-layersizes[num_layer]);
                copyRecurrentAc ();

                if (flag_intplt)
                {
                    if (fscanf (fptr_nglm, "%f\n", &prob_ng) != 1)
                    {
                        printf ("ERROR: Failed to read ngram prob from ng stream file!\n");
                        exit (0);
                    }
                }
                prob_int = lambda*prob_rnn + (1-lambda)*prob_ng;
                if (inputmap.find(word) == inputmap.end())
                {
                    prevword = inOOSindex;
                }
                else
                {
                    prevword = inputmap[word];
                }
                logp_rnn += log10(prob_rnn);
                logp_ng  += log10(prob_ng);
                logp_int += log10(prob_int);
                sentlogp += log10(prob_int);
                wordcn ++;
                if (debug == 1)
                {
                    printf ("%f ", log10(prob_int));
                }
                if (debug > 1)
                {
                    if (flag_intplt)
                        printf ("%d\t%.10f\t%.10f\t%.10f\t%s", curword, prob_rnn, prob_ng, prob_int, word.c_str());
                    else
                        printf ("%d\t%.10f\t%s", curword, prob_rnn, word.c_str());
                    if (curword == outOOSindex)
                    {
                        printf ("<OOS>");
                    }
                    printf ("\n");
                }
                if (debug > 1)
                {
                    if (wordcn % 10000 == 0)
                    {
                        float nwordspersec = wordcn / (timer.stop());
                        printf ("eval speed  %.4f Words/sec\n", nwordspersec);
                    }
                }
            }
            sentcnt ++;
            if (debug == 1)
            {
                printf ("sent=%f %d\n", sentlogp, sentcnt);
            }
            if (debug == 0)
            {
                printf ("%f\n", sentlogp);
            }
            fflush(stdout);
            prevnbestid = nbestid;
        }
    }
    if (debug > 1)
    {
        float nwordspersec = wordcn / (timer.stop());
        printf ("eval speed  %.4f Words/sec\n", nwordspersec);
    }
    ppl_rnn = exp10(-logp_rnn/(wordcn));
    ppl_ng  = exp10(-logp_ng/(wordcn));
    ppl_int = exp10(-logp_int/(wordcn));
    if (debug > 1)
    {
        if (flag_intplt)
        {
            printf ("Total word: %d\n", wordcn);
            printf ("N-Gram log probability: %.3f\n", logp_ng);
            printf ("RNNLM  log probability: %.3f\n", logp_rnn);
            printf ("Intplt log probability: %.3f\n\n", logp_int);
            printf ("N-Gram PPL : %.3f\n", ppl_ng);
            printf ("RNNLM  PPL : %.3f\n", ppl_rnn);
            printf ("Intplt PPL : %.3f\n", ppl_int);
        }
        else
        {
            printf ("Total word: %d\n", wordcn);
            printf ("Average logp: %f\n", logp_rnn/log10(2)/wordcn);
            printf ("RNNLM  log probability: %.3f\n", logp_rnn);
            printf ("RNNLM  PPL : %.3f\n", ppl_rnn);
        }
    }
    fileptr.close();

    if (fptr_nglm)
    {
        fclose(fptr_nglm);
    }
    return SUCCESS;
}

bool RNNLM::sample (string textfile, string unigramfile, int n)
{
    int i, mb, word, sentlen;
    float g, f;
    string wordstr;
    ReadUnigramFile (unigramfile);
    sampletextfile = textfile;
    uglmfile = unigramfile;
    nsample = n;
    neu_ac[1]->assignmatvalue(RESETVALUE);
    cucopyHiddenLayerToInput();
    FILE **fptr_txts;
    fptr_txts = (FILE **) malloc (sizeof(FILE *) * minibatch);
    char txtfilestr[1024][1024];
    if (debug > 1)
    {
        printSampleInfo ();
    }
    for (int i=0; i<minibatch; i++)
    {
        sprintf (txtfilestr[i], "%s.mb%d", textfile.c_str(), i);
        fptr_txts[i] = fopen (txtfilestr[i], "w");
    }
    for (i=0; i<minibatch; i++)
    {
        host_prevwords[i] = inStartindex;
        host_curwords[i] = outEndindex;
    }
    cucpytoGpu (dev_prevwords, host_prevwords, minibatch*sizeof(int));
    cucpytoGpu (dev_curwords, host_curwords, minibatch*sizeof(int));
    wordcn = 0;
    int nvalidmbsize = minibatch;

    int *dev_samples = (int *)cucalloc (minibatch*sizeof(int));
    int *host_sample = (int *)calloc(minibatch, sizeof(int));
    float *dev_randv = (float *)cucalloc(minibatch*sizeof(float));
    float *host_randv = (float *)calloc(minibatch, sizeof(float));

    // while (wordcn < nsample)
    while (nvalidmbsize > 0)
    {
        cuForward ();
        cucopyHiddenLayerToInput();
#if 1
        // using GPU for sampling
        for (i=0; i<minibatch; i++)
        {
            host_randv[i] = randomv(0, 1);
        }
        cucpytoGpu(dev_randv, host_randv, minibatch*sizeof(float));

        neu_ac[num_layer]->sample(dev_samples, dev_randv, minibatch);

        cucpyfromGPU (host_sample, dev_samples, minibatch*sizeof(int));
        for (int mb=0; mb<minibatch; mb++)
        {
            if (noupdateset.find(mb) != noupdateset.end())
            {
                continue;
            }
            word = host_sample[mb];
            if (word == outOOSindex)
            {
                f = randomv(0, 1);
                g = 0;
                i = 0;
                while ((g<f) && (i < num_oosword))
                {
                    g += ooswordsprob[i];
                    i ++;
                }
                word = i - 1;
                wordstr = ooswordsvec[word].c_str();
                if (inputmap.find(wordstr) == inputmap.end())
                {
                    host_prevwords[mb] = inOOSindex;
                }
                else
                {
                    host_prevwords[mb] = inputmap[wordstr];
                }
                fprintf (fptr_txts[mb], " %s", wordstr.c_str());
            }
            else if (word != outEndindex)
            {
                wordcn ++;

                wordstr = outputvec[word].c_str();
                if (inputmap.find(wordstr) == inputmap.end())
                {
                    host_prevwords[mb] = inOOSindex;
                }
                else
                {
                    host_prevwords[mb] = inputmap[wordstr];
                }
                fprintf (fptr_txts[mb], " %s", wordstr.c_str());
            }
            else
            {
                host_prevwords[mb] = inStartindex;
                wordstr = "\n";
                if (wordcn > nsample)
                {
                    host_prevwords[mb] = INVALID_INT;
                    host_curwords[mb] = INVALID_INT;
                    nvalidmbsize --;
                }
                fprintf (fptr_txts[mb], "%s", wordstr.c_str());
            }
        }
        cucpytoGpu (dev_prevwords, host_prevwords, minibatch*sizeof(int));
#else
        // using CPU for sampling
        neu_ac[num_layer]->fetch();
        for (int mb=0; mb<minibatch; mb++)
        {
            if (noupdateset.find(mb) != noupdateset.end())
            {
                continue;
            }
            f = randomv (0, 1);
            g = 0;
            i = 0;

            while ((g < f) && (i < layersizes[num_layer]))
            {
                g += neu_ac[num_layer]->fetchhostvalue(i, mb);
                i ++;
            }
            word = i-1;

            if (word == outOOSindex)
            {
                f = randomv(0, 1);
                g = 0;
                i = 0;
                while ((g<f) && (i < num_oosword))
                {
                    g += ooswordsprob[i];
                    i ++;
                }
                word = i - 1;
                wordstr = ooswordsvec[word].c_str();
                if (inputmap.find(wordstr) == inputmap.end())
                {
                    host_prevwords[mb] = inOOSindex;
                }
                else
                {
                    host_prevwords[mb] = inputmap[wordstr];
                }
                fprintf (fptr_txts[mb], " %s", wordstr.c_str());
            }
            else if (word != outEndindex)
            {
                wordcn ++;

                wordstr = outputvec[word].c_str();
                if (inputmap.find(wordstr) == inputmap.end())
                {
                    host_prevwords[mb] = inOOSindex;
                }
                else
                {
                    host_prevwords[mb] = inputmap[wordstr];
                }
                fprintf (fptr_txts[mb], " %s", wordstr.c_str());
            }
            else
            {
                host_prevwords[mb] = inStartindex;
                wordstr = "\n";
                if (wordcn > nsample)
                {
                    host_prevwords[mb] = INVALID_INT;
                    host_curwords[mb] = INVALID_INT;
                    nvalidmbsize --;
                }
                fprintf (fptr_txts[mb], "%s", wordstr.c_str());
            }
        }
        cucpytoGpu (dev_prevwords, host_prevwords, minibatch*sizeof(int));
#endif
    }

    char *line = (char *)malloc(MAX_WORD_LINE * sizeof(char));
    int max_words_line = MAX_WORD_LINE;
    FILE *fptr = fopen (textfile.c_str(), "w");
    for (i=0; i<minibatch; i++)
    {
        fclose (fptr_txts[i]);
        fptr_txts[i] = NULL;
        fptr_txts[i] = fopen(txtfilestr[i], "r");
        if (fptr_txts[i] == NULL)
        {
            printf ("failed to open text file: %s\n", txtfilestr[i]);
            exit (0);
        }
        while (!feof(fptr_txts[i]))
        {
            sentlen = getline (line, max_words_line, fptr_txts[i]);
            if (sentlen > 1)
            {
                fprintf (fptr, "%s", line);
            }
        }
        fclose (fptr_txts[i]);
        if (remove(txtfilestr[i]) == 0)
        {
            printf ("Removed %s.\n", txtfilestr[i]);
        }
        else
        {
            printf ("ERROR: failed to remove %s\n", txtfilestr[i]);
        }
        fflush(stdout);
        fflush(fptr);
    }
    fclose (fptr);
    if (line)  free (line);
    return SUCCESS;
}

void RNNLM::ReadUnigramFile (string unigramfile)
{
    FILE *fin = fopen (unigramfile.c_str(), "r");
    char line[MAX_STRING];
    char line2[MAX_STRING];
    float prob = 0.0, logprob = 0.0;
    float accprob = 0.0;
    char word[MAX_STRING];
    ooswordsvec.clear();
    ooswordsprob.clear();
    fscanf (fin, "%s\n", line);  /*  \data\  */
    fscanf (fin, "%s%s\n", line, line2);
    fscanf (fin, "%sgram:\n", line);
    int i = 0;
    while (! feof(fin))
    {
        if (fscanf (fin, "%f%s", &logprob, word) == 2)
        {
            prob = pow(10, logprob);
            if ((outputmap.find(word) == outputmap.end()) && (word[0] != '!'))
            {
                accprob += prob;
                ooswordsvec.push_back(string(word));
                ooswordsprob.push_back(prob);
                i ++;
            }
        }
        else
        {
            fscanf (fin, "%s", word);
        }
    }
    num_oosword = ooswordsprob.size();
    fclose (fin);

    for (i=0; i<num_oosword; i++)
    {
        ooswordsprob[i] = ooswordsprob[i] / accprob;
    }
}

void RNNLM::preprocessforward ()
{
    for (int mb=0; mb<minibatch; mb++)
    {
        if (host_curwords[mb] == INVALID_INT)
        {
            if (noupdateset.find(mb) == noupdateset.end())
            {
                assert (host_prevwords[mb] == INVALID_INT);
                noupdateset.insert(mb);
                host_prevwords[mb] = 0;
                host_curwords[mb]  = 0;
                cucpytoGpu (dev_prevwords+mb, host_prevwords+mb, sizeof(int));
                cucpytoGpu (dev_curwords+mb, host_curwords+mb, sizeof(int));
            }
        }
        else if (independent && host_prevwords[mb]==inStartindex && wordcn!=0 && noupdateset.find(mb)==noupdateset.end())      // start of sentence
        {
            neu_ac[1]->assigndevcolumnvalue (mb, RESETVALUE);
            neu0_ac_hist->assigndevcolumnvalue (mb, RESETVALUE);
            if (bptt > 0)
            {
                for (int a = 0; a < bptt+bptt_delay; a ++)
                {
                    bptt_hiddenac[a]->assigndevcolumnvalue (mb, 0.0);
                    bptt_hiddener[a]->assigndevcolumnvalue (mb, 0.0);
                    cumemset (&dev_bptt_history[mb+minibatch*a], 0, sizeof(int));
                    memset (&host_bptt_history[mb+minibatch*a], 0, sizeof(int));
                }
            }
        }
        if ((dim_fea>0) && (host_prevwords[mb]==inStartindex) && wordcn!=0 && noupdateset.find(mb)==noupdateset.end())
        {
            mbfeaindices[mb] += minibatch;
            int index = feaindices[mbfeaindices[mb]];
            cucpyInGpu (neu0_ac_fea->getdevdataptr(0, mb), feamatrix->getdevdataptr(0, index), sizeof(float)*dim_fea);
        }
    }
}

void RNNLM::cuForward (int evalmode /*=0*/)
{
    int i;
    GPUsynchronizewithCPU ();
    preprocessforward ();

    // this operation seems unncecessary
    // neu_ac[0]->assignneu0ac (dev_prevwords, minibatch, 1.0);

    for (i=0; i<num_layer; i++)
    {
        if (i==0)
        {
            // neu0 -> neu1
#ifdef DEBUG
            timer_hidden.start();
#endif
#ifdef INPUTEMBEDDINGCPU
            forwardInputWordWgts();
            cumatrixXmatrix (layer0_hist, neu0_ac_hist, neu_ac[i+1], true, false, 1.0, 1.0);
#else
            cumatrixXmatrix (layer0_hist, neu0_ac_hist, neu_ac[i+1], true, false);
            if (dim_fea > 0)
            {
                cumatrixXmatrix (layer0_fea, neu0_ac_fea, neu_ac[i+1], true, false, 1.0, 1.0);
            }
            neu1addneu0words (dev_prevwords, neu_ac[i+1]->getdevdataptr(), layers[i]->getdevdataptr(), layersizes[i], layersizes[i+1], minibatch);
#endif
            neu_ac[i+1]->sigmoid ();
#ifdef DEBUG
            timer_hidden.add();
#endif
        }
        else
        {
            if (i == num_layer-1 && traincritmode==2 && evalmode==0)
                // NCE training in the output layer
            {
#ifdef DEBUG
                timer_forward.start();
#endif
                forward_NCE ();
#ifdef DEBUG
                timer_forward.add();
#endif
                return;
            }
            if (i == num_layer-1 && nclass > 0)
                // class layer used in the output layer
            {
                // forward for class layer
                cumatrixXmatrix (layerN_class, neu_ac[i], neuN_ac_class, true, false);
                neuN_ac_class->softmax(lognorms);
                // forward for word layer
                // TODO: optimize here
                layers[i]->forwardWordlayer (neu_ac[i], neu_ac[i+1], dev_curclass, dev_classinfo);
                neu_ac[i+1]->softmaxWordlayer (dev_curclass, dev_classinfo);
            }
            else
            {
                cumatrixXmatrix (layers[i], neu_ac[i], neu_ac[i+1], true, false);
                if (i == num_layer-1)   neu_ac[i+1]->softmax(lognorms);
                else                    neu_ac[i+1]->sigmoid();
            }
        }
    }

    if (traincritmode == 1 || traincritmode == 2 )         // variance regularization
    {
        lognorms->fetch();
        if (evalmode == 0)          // training stage
        {
            int effnum = 0;
            lognorm = 0;
            for (int mb=0; mb<minibatch; mb++)
            {
                if (noupdateset.find(mb) == noupdateset.end())
                {
                    lognorm += lognorms->fetchhostvalue(0, mb);
                    effnum  += 1;
                }
            }
            lognorm = lognorm / effnum;
            for (int mb=0; mb<minibatch; mb++)
            {
                if (noupdateset.find(mb) == noupdateset.end())
                {
                    float v = lognorms->fetchhostvalue(0, mb);
                    trnlognorm_mean += v;
                    v = v- lognorm;
                    lognorms->assignhostvalue(0, mb, v);
                }
            }
            lognorms->assign();
        }
        else                        // evaluate stage
        {
            for (int mb=0; mb<minibatch; mb++)
            {
                if (noupdateset.find(mb) == noupdateset.end())
                {
                    float v = lognorms->fetchhostvalue(0, mb);
                    lognorm_mean += v;
                    lognorm_var  += v*v ;
                }
            }
        }
    }
    GPUsynchronizewithCPU ();
}

void RNNLM::cuBpttAndUpdate ()
{
    int i, a, step;
    GPUsynchronizewithCPU ();
    if (traincritmode == 0)
    {
        if (nclass > 0)
        // for class layer based output layer
        {
            neuN_er_class->calerronoutputlayer (neuN_ac_class, dev_curclass);
            neu_er[num_layer]->calerronWordlayer(neu_ac[num_layer], dev_curclass, dev_curwords, dev_classinfo);
        }
        else
        {
            neu_er[num_layer]->calerronoutputlayer (neu_ac[num_layer], dev_curwords);
        }
    }
    else if (traincritmode == 1)         // variance regularization
    {
        neu_er[num_layer]->calerronoutputlayer_vr (neu_ac[num_layer], dev_curwords, lognorms, vrpenalty);
    }
    else if (traincritmode == 2)
    {
#ifdef DEBUG
        timer_output.start();
#endif
        calerr_NCE ();
#ifdef DEBUG
        timer_output.add();
#endif
    }

    // disable the part of sample which has arrived the end of file.
    if (noupdateset.size()>0 && traincritmode!=2)
    {
        for (set<int>::iterator it = noupdateset.begin(); it != noupdateset.end(); it ++)
        {
            int mbindex = *it;
            assert (host_curwords[mbindex] == 0);
            cumemset (neu_er[num_layer]->getdevdataptr(0, mbindex), 0, layersizes[num_layer]*sizeof(float));
            if (nclass > 0)
            {
                cumemset(neuN_er_class->getdevdataptr(0, mbindex), 0, nclass*sizeof(float));
            }
        }
    }

    for (i=num_layer; i>1; i--)
    {
        if (i != num_layer)
        {
            neu_er[i]->multiplysigmoid (neu_ac[i]);
        }
        neu_er[i-1]->initmatrix (0);
        if (i==num_layer && traincritmode==2)
        {
#ifdef DEBUG
            timer_backprop.start();
#endif
            bpttAndupdate_NCE ();
#ifdef DEBUG
            timer_backprop.add();
#endif
        }
        else
        {
            // error backpropagation
            if (nclass > 0)
            {
                // error from class layer
                cumatrixXmatrix (layerN_class, neuN_er_class, neu_er[i-1], false, false, 1.0, 0.0);
                // error from Word layer
                bperWordlayer (layers[num_layer-1], neu_er[num_layer], neu_er[num_layer-1], dev_curclass, dev_classinfo, 1.0, 1.0);
            }
            // for class based output layer
            else
            {
                cumatrixXmatrix (layers[i-1], neu_er[i], neu_er[i-1], false, false, 1.0, 0.0);
            }
            // update weight
            if (nclass > 0)
            {
                // for class layer
                cumatrixXmatrix (neu_ac[i-1], neuN_er_class, layerN_class, false, true, alpha, 1.0);
                // for word layer
                bpupdateWordlayer (neu_ac[num_layer-1], neu_er[num_layer], layers[num_layer-1], dev_curclass, dev_classinfo, alpha, 1.0);
            }
            else
            {
                if (lrtunemode == 0)        //for SGD
                {
                    cumatrixXmatrix (neu_ac[i-1], neu_er[i], layers[i-1], false, true, alpha, 1.0);
                }
                else                        // for adagrad
                {
                    cumatrixXmatrix (neu_ac[i-1], neu_er[i], gradlayers[i-1], false, true, 1.0, 0.0);
                    accgradlayers[i-1]->addsquaregrad (gradlayers[i-1], alpha);
                    layers[i-1]->addgrad (gradlayers[i-1]);
                }
            }
            if (gradient_cutoff > 0)
            {
                neu_er[i-1]->gradcutoff (gradient_cutoff);
            }
        }
    }

#ifdef DEBUG
    timer_hidden.start();
#endif
    if (bptt <= 1)
    {
        // er = er * ac * (1-ac)
        neu_er[1]->multiplysigmoid (neu_ac[1]);
        if (lrtunemode == 0)        // for SGD
        {
            cumatrixXmatrix (neu0_ac_hist, neu_er[1], layer0_hist, false, true, alpha, 1.0);
            if(dim_fea > 0)
            {
                cumatrixXmatrix (neu0_ac_fea, neu_er[1], layer0_fea, false, true, alpha, 1.0);
            }
#if 1
#ifdef INPUTEMBEDDINGCPU
            updateInputWordWgts (host_prevwords);
#else
            layers[0]->updatelayer0_word (neu_er[1], dev_prevwords, alpha);
#endif
#endif
        }
        else                        // for adagrad
        {
            // update recurrent connection
            cumatrixXmatrix (neu0_ac_hist, neu_er[1], gradlayer0_hist, false, true, 1.0, 0.0);
            accgradlayer0_hist->addsquaregrad (gradlayer0_hist, alpha);
            layer0_hist->addgrad (gradlayer0_hist);
            // update word part
#if 0
            layers[0]->updatelayer0_word (neu_er[1], dev_prevwords, alpha);
#else
#ifdef INPUTEMBEDDINGCPU
            updateInputWordWgts (host_prevwords);
#else
            gradlayers[0]->updatelayer0_word (neu_er[1], dev_prevwords, 1.0);
            set<int> prevwordset;
            for (int i=0; i<minibatch; i++)
            {
                if (prevwordset.find(host_prevwords[i]) == prevwordset.end())
                {
                    prevwordset.insert (i);
                }
                else
                {
                    host_prevwords[i] = FILLEDNULL;
                }
            }
            cucpytoGpu (dev_prevwords, host_prevwords, sizeof(int)*minibatch);
            accgradlayers[0]->addsquaregrad_word (gradlayers[0], dev_prevwords, alpha, minibatch);
            layers[0]->addgrad_word (gradlayers[0], dev_prevwords, minibatch);
#endif
#endif
        }
    }
    else
    {
        bptt_hiddenac[0]->assign (neu_ac[1]);
        bptt_hiddener[0]->assign (neu_er[1]);
        if (independent || (counter % bptt_delay) == 0)
        {
            for (step=0; step<bptt+bptt_delay-2; step++)
            {
                // er = er * ac * (1-ac)
                neu_er[1]->multiplysigmoid (neu_ac[1]);
                // update
#ifdef INPUTEMBEDDINGCPU
                updateInputWordWgts (host_bptt_history+(step*minibatch));
#else
                layers[0]->updatelayer0_word (neu_er[1], dev_bptt_history + (step*minibatch), alpha);
#endif
                if(step==0 && dim_fea>0)
                {
                    cumatrixXmatrix (neu0_ac_fea, neu_er[1], layer0_fea, false, true, alpha, 1.0);
                }

                // clean subneu0_er_hist
                neu0_er_hist->initmatrix (0);

                // back propogate error. neu1 -> neu0_hist
                cumatrixXmatrix (layer0_hist, neu_er[1], neu0_er_hist, false, false);
                if (gradient_cutoff > 0)
                {
                    neu0_er_hist->gradcutoff (gradient_cutoff);
                }
                // update weight matrix associate with hist in input layer: layer0
                cumatrixXmatrix (neu0_ac_hist, neu_er[1], bptt_layer0_hist, false, true, alpha, 1.0);

                 neu_er[1]->assign (neu0_er_hist);
                 neu_er[1]->add (bptt_hiddener[step+1]);

                 // assign neu1_ac and neu0_ac_hist for bp in next loop
                if (step < bptt + bptt_delay - 3)
                {
                    neu_ac[1]->assign (bptt_hiddenac[step+1]);
                    neu0_ac_hist->assign (bptt_hiddenac[step+2]);
                }
           }

            // clear bptt_hiddener
            for (a=0; a< bptt+bptt_delay+1; a ++)
            {
                bptt_hiddener[a]->initmatrix(0);
            }
            // restore hidden layer after bptt
            // really need the function to copy data in GPU directly!!
            neu_ac[1]->assign (bptt_hiddenac[0]);

            // update the weight matrix in layer0
            layer0_hist->addsubmatrix (bptt_layer0_hist, 0, 0, layersizes[1], layersizes[1]);
            bptt_layer0_hist->initmatrix(0);
        }
    }
#ifdef DEBUG
    timer_hidden.add();
#endif
}

void RNNLM::cucopyHiddenLayerToInput()
{
    cucpyInGpu (neu0_ac_hist->getdevdataptr(), neu_ac[1]->getdevdataptr(), layersizes[1]*minibatch*sizeof(float));
}
void RNNLM::cuBpttOperation ()
{
    int a;
    // neu_ac[0]->assignneu0ac (dev_prevwords, minibatch, 0.0);
    if (bptt > 0)
    {
        for (a=bptt+bptt_delay-1; a>0; a--)
        {
            cucpyInGpu (dev_bptt_history+a*minibatch, dev_bptt_history + (a-1)*minibatch, minibatch* sizeof(int));
            memcpy (host_bptt_history+a*minibatch, host_bptt_history+(a-1)*minibatch, minibatch*sizeof(int));
        }
        cucpyInGpu (dev_bptt_history, dev_prevwords, minibatch*sizeof(int));
        memcpy (host_bptt_history, host_prevwords, minibatch*sizeof(int));
        for (a = bptt+bptt_delay-1; a>0; a--)
        {
            cucpyInGpu (bptt_hiddenac[a]->getdevdataptr(), bptt_hiddenac[a-1]->getdevdataptr(), bptt_hiddenac[a]->Sizeof());
            cucpyInGpu (bptt_hiddener[a]->getdevdataptr(), bptt_hiddener[a-1]->getdevdataptr(), bptt_hiddener[a]->Sizeof());
        }
    }
}

void RNNLM::InitVariables ()
{
    int i, j;
    counter = 0;
    logp = 0;
    wordcn = 0;
    cumemset (dev_bptt_history, 0, (bptt+bptt_delay)*minibatch*sizeof(int));
    memset (host_bptt_history, 0, (bptt+bptt_delay)*minibatch*sizeof(int));
    if (independent)
    {
        for (i=0; i<neu0_ac_hist->rows(); i++)
            for (j=0; j<neu0_ac_hist->cols(); j++)
                neu0_ac_hist->assignhostvalue(i, j, RESETVALUE);
        neu0_ac_hist->assign();
    }
    for (i=0; i<bptt+bptt_delay+1; i++)
    {
        bptt_hiddenac[i]->initmatrix(0);
        bptt_hiddener[i]->initmatrix(0);
    }
    noupdateset.clear();
    if (traincritmode == 1 || traincritmode == 2)
    {
        lognorm_mean = 0;
        lognorm_var  = 0;
    }
}

void RNNLM::CollectEntropy (int evalmode /* =0 */)
{

    if (traincritmode == 2 && evalmode == 0)    // for NCE and train stage
    {
#ifdef DEBUG
        int n;
        float word_prob;
        for (n=0; n<N; n++)
        {
            int sample = ncesample[n];
            int mbindex = n/(k+1);
            if (noupdateset.find(mbindex) == noupdateset.end())
            {
                float ac = neu_ac[num_layer]->fetchhostvalue(sample, mbindex);
                if (n%(k+1) == 0) // word
                {
                    logp += log(ac/(ac+k*unigram[sample])+1e-9);
                    wordcn ++;
                }
                else                         // noise
                {
                    logp += log(k*unigram[sample]/(ac+k*unigram[sample])+1e-9);
                }
            }
        }
#else
        for (int mb=0; mb<minibatch; mb++)
        {
            if (noupdateset.find(mb) == noupdateset.end())
            {
                wordcn ++;
            }
        }
        logp = 0;
#endif
        return;
    }
    for (int mb=0; mb<minibatch; mb ++)
    {
        if (noupdateset.find(mb) == noupdateset.end())  // mb not in noupdateset
        {
            int word = host_curwords[mb];
            float word_prob;
            if (nclass > 0)
            {
                int clsid = host_curclass[mb];
                word_prob = neu_ac[num_layer]->fetchvalue(word, mb)*neuN_ac_class->fetchvalue(clsid, mb);
            }
            else
            {
                word_prob = neu_ac[num_layer]->fetchvalue (word, mb);
                if (traincritmode==2 && word == outOOSindex)
                {
                	word_prob /= (fullvocsize - layersizes[num_layer]);
                }
            }
            if (isinf (log10(word_prob))|| word_prob == 0)
            {
                if (debug > 2)
                {
                    printf("\nNumerical error %dth sample in %dth minibatch, word_prob=%f\n", mb, counter, neu_ac[num_layer]->fetchvalue (word, mb));
                }
            }
            else
            {
                logp += log10(word_prob);
                wordcn ++;
            }
        }
    }
}

void RNNLM::SelectDevice ()
{
    dumpdeviceInfo ();
    int deviceNum = numdevices();
    if (deviceid < 0)
    {
        printf ("ERROR: gpu device id (%d) should be greater than 0\n", deviceid);
    }
    if (deviceid > deviceNum-1)
    {
        printf ("ERROR: deviceid(%d) should be less than deviceNum(%d)\n", deviceid, deviceNum);
        exit(0);
    }
    setDeviceid (deviceid);
    if (debug > 1)
    {
        printf ("Device %d will be chosen for training\n", deviceid);
    }
    // init cublas
    initcuHandle ();
}

void RNNLM::LoadRNNLM(string modelname)
{
    if (binformat)
    {
        LoadBinaryRNNLM(modelname);
    }
    else
    {
        LoadTextRNNLM(modelname);
    }
}

void RNNLM::LoadTextRNNLM (string modelname)
{
    int i, a, b;
    float v;
    char word[1024];
    FILE *fptr = NULL;
    // read model file
    fptr = fopen (modelname.c_str(), "r");
    if (fptr == NULL)
    {
        printf ("ERROR: Failed to read RNNLM model file(%s)\n", modelname.c_str());
        exit (0);
    }
    fscanf (fptr, "cuedrnnlm v%f\n", &v);
    if (v != version)
    {
        printf ("Error: the version of rnnlm model(v%.1f) is not consistent with binary supported(v%.1f)\n", v, version);
        exit (0);
    }
    fscanf (fptr, "train file: %s\n", word);     trainfile = word;
    fscanf (fptr, "valid file: %s\n", word);     validfile = word;
    fscanf (fptr, "number of iteration: %d\n", &iter);
    fscanf (fptr, "#train words: %d\n", &trainwordcnt);
    fscanf (fptr, "#valid words: %d\n", &validwordcnt);
    fscanf (fptr, "#layer: %d\n", &num_layer);
    layersizes.resize(num_layer+1);
    for (i=0; i<layersizes.size(); i++)
    {
        fscanf (fptr, "layer %d size: %d\n", &b, &a);
        assert(b==i);
        layersizes[i] = a;
    }
    fscanf (fptr, "feature dimension: %d\n", &dim_fea);
    fscanf (fptr, "class layer dimension: %d\n", &nclass);
    allocMem (layersizes);

    fscanf (fptr, "independent mode: %d\n", &independent);
    fscanf (fptr, "train crit mode: %d\n",  &traincritmode);
    for (i=0; i<num_layer; i++)
    {
        fscanf (fptr, "layer %d -> %d\n", &a, &b);
        assert (a==i);
        assert (b==(i+1));
        for (a=0; a<layersizes[i]; a++)
        {
            for (b=0; b<layersizes[i+1]; b++)
            {
                fscanf (fptr, "%f", &v);
                layers[i]->assignhostvalue(a, b, v);
            }
            fscanf (fptr, "\n");
        }
    }
    fscanf (fptr, "recurrent layer 1 -> 1\n");
    for (a=0; a<layersizes[1]; a++)
    {
        for (b=0; b<layersizes[1]; b++)
        {
            fscanf (fptr, "%f", &v);
            layer0_hist->assignhostvalue(a, b, v);
        }
        fscanf (fptr, "\n");
    }
    if (dim_fea > 0)
    {
        fscanf (fptr, "feature layer weight\n");
        for (a=0; a<dim_fea; a++)
        {
            for (b=0; b<layersizes[1]; b++)
            {
                fscanf (fptr, "%f", &v);
                layer0_fea->assignhostvalue(a, b, v);
            }
            fscanf (fptr, "\n");
        }
    }
    if (nclass > 0)
    {
        fscanf (fptr, "class layer weight\n");
        for (a=0; a<layersizes[num_layer-1]; a++)
        {
            for (b=0; b<nclass; b++)
            {
                fscanf (fptr, "%f", &v);
                layerN_class->assignhostvalue(a, b, v);
            }
            fscanf (fptr, "\n");
        }
    }
    fscanf (fptr, "hidden layer ac\n");
    for (a=0; a<layersizes[1]; a++)
    {
        fscanf (fptr, "%f", &v);
        for (b=0; b<minibatch; b++)     neu0_ac_hist->assignhostvalue(a, b, v);
    }
    fscanf (fptr, "\n");
    fscanf (fptr, "%d", &a);
    if (a != CHECKNUM)
    {
        printf ("ERROR: failed to read the check number(%d) when reading model\n", CHECKNUM);
        exit (0);
    }
    if (debug > 1)
    {
        printf ("Successfully loaded model: %s\n", modelname.c_str());
    }
    fclose (fptr);

    if (flag_usegpu)
    {
        cpyRnnLMtoGPU ();
    }
}

void RNNLM::LoadBinaryRNNLM(string modelname)
{
    int i, a, b;
    float v;
    char word[1024];
    FILE *fptr = NULL;
    fptr = fopen (modelname.c_str(), "rb");
    if (fptr == NULL)
    {
        printf ("ERROR: Failed to read RNNLM model file(%s)\n", modelname.c_str());
        exit (0);
    }
    fread (&v, sizeof(float), 1, fptr);
    if (v != version)
    {
        printf ("Error: the version of rnnlm model(v%.1f) is not consistent with binary supported(v%.1f)\n", v, version);
        exit (0);
    }
    fread (&iter, sizeof(int), 1, fptr);
    fread (&num_layer, sizeof(int), 1, fptr);
    layersizes.resize(num_layer+1);
    for (i=0; i<layersizes.size(); i++)
    {
        fread (&a, sizeof(int), 1, fptr);
        layersizes[i] = a;
    }
    fread (&dim_fea, sizeof(int), 1, fptr);
    fread (&nclass, sizeof(int), 1, fptr);
    allocMem (layersizes);

    fread (&independent, sizeof(int), 1, fptr);
    fread (&traincritmode, sizeof(int), 1, fptr);

    for (i=0; i<num_layer; i++)
    {
        for (a=0; a<layersizes[i]; a++)
        {
            for (b=0; b<layersizes[i+1]; b++)
            {
                fread (&v, sizeof(float), 1, fptr);
                layers[i]->assignhostvalue(a, b, v);
            }
        }
    }
    for (a=0; a<layersizes[1]; a++)
    {
        for (b=0; b<layersizes[1]; b++)
        {
            fread (&v, sizeof(float), 1, fptr);
            layer0_hist->assignhostvalue(a, b, v);
        }
    }
    if (dim_fea > 0)
    {
        for (a=0; a<dim_fea; a++)
        {
            for (b=0; b<layersizes[1]; b++)
            {
                fread (&v, sizeof(float), 1, fptr);
                layer0_fea->assignhostvalue(a, b, v);
            }
        }
    }
    if (nclass > 0)
    {
        for (a=0; a<layersizes[num_layer-1]; a++)
        {
            for (b=0; b<nclass; b++)
            {
                fread (&v, sizeof(float), 1, fptr);
                layerN_class->assignhostvalue(a, b, v);
            }
        }
    }
    for (a=0; a<layersizes[1]; a++)
    {
        fread (&v, sizeof(float), 1, fptr);
        neu_ac[1]->assignhostvalue(a, 0, v);
    }

    fread (&a, sizeof(int), 1, fptr);
    if (a != CHECKNUM)
    {
        printf ("ERROR: failed to read the check number(%d) when reading model\n", CHECKNUM);
        exit (0);
    }
    if (debug > 0)
    {
        printf ("Successfully loaded model: %s\n", modelname.c_str());
    }
    fclose (fptr);
    cpyRnnLMtoGPU ();
}

void RNNLM::WriteRNNLM_oldformat(string modelname)
{
    int i, a, b, version=10, filetype=0, train_cur_pos=0, logp=0,
        anti_k=0, train_words=0, TEXT=0, direct_size=0;
    float llogp = 0;
    bool flag_oos = true;
    cpyRnnLMtoGPU ();
    FILE *fo = NULL;
    fo = fopen (modelname.c_str(), "w");
    if (fo == NULL)
    {
        printf ("ERROR: Failed to create RNNLM model file(%s)\n", modelname.c_str());
        exit (0);
    }
    fprintf(fo, "version: %d\n", version);
    fprintf(fo, "file format: %d\n\n", filetype);

    fprintf(fo, "training data file: %s\n", trainfile.c_str());
    fprintf(fo, "validation data file: %s\n\n", validfile.c_str());

    fprintf(fo, "last probability of validation data: %f\n", llogp);
    fprintf(fo, "number of finished iterations: %d\n", iter);

    fprintf(fo, "current position in training data: %d\n", train_cur_pos);
    fprintf(fo, "current probability of training data: %f\n", logp);
    fprintf(fo, "save after processing # words: %d\n", anti_k);
    fprintf(fo, "# of training words: %d\n", train_words);

    fprintf(fo, "input layer size: %d\n", layersizes[0]+layersizes[1]);
    fprintf(fo, "hidden layer size: %d\n", layersizes[1]);
    fprintf(fo, "compression layer size: %d\n", 0);
    fprintf(fo, "output layer size: %d\n", layersizes[2]);

    fprintf(fo, "direct connections: %lld\n", 0);
    fprintf(fo, "direct order: %d\n", 3);

    fprintf(fo, "bptt: %d\n", 5);
    fprintf(fo, "bptt block: %d\n", 10);

    fprintf(fo, "vocabulary size: %d\n", -1);

    fprintf(fo, "independent sentences mode: %d\n", independent);

    fprintf(fo, "starting learning rate: %f\n", alpha);
    fprintf(fo, "current learning rate: %f\n", alpha);
    fprintf(fo, "learning rate decrease: %d\n", alpha_divide);
    fprintf(fo, "\n");


    fprintf (fo, "\nflag_oos:true\n");
    fprintf (fo, "\ninputlist size(include <s> and <OOS>):%d\n", inputmap.size());
    fprintf (fo, "\noutputlist size(include <s> and <OOS>):%d\n", outputmap.size());

    fprintf(fo, "\nHidden layer activation:\n");
    for (a=0; a<layersizes[1]; a++) fprintf(fo, "%.4f\n", neu0_ac_hist->fetchhostvalue(a, 0));
    //////////
    fprintf(fo, "\nWeights 0->1:\n");
    for (b=0; b<layersizes[1]; b++)
    {
        for (a=0; a<layersizes[0]; a++)
        {
            fprintf (fo, "%.4f\n", layers[0]->fetchhostvalue(a, b));
        }
        for (a=0; a<layersizes[1]; a++)
        {
            fprintf (fo, "%.4f\n", layer0_hist->fetchhostvalue(a, b));
        }
    }
    /////////
    fprintf(fo, "\n\nWeights 1->2:\n");
    for (b=0; b<layersizes[2]; b++)
    {
        for (a=0; a<layersizes[1]; a++)
        {
            fprintf(fo, "%.4f\n", layers[1]->fetchhostvalue(a, b));
        }
    }

    fprintf(fo, "\nDirect connections:\n");
    fclose(fo);
}

void RNNLM::WriteRNNLM(string modelname)
{
    // write to input and output word list
    string inputlist = outmodelfile + ".input.wlist.index";
    string outputlist = outmodelfile + ".output.wlist.index";
    WriteWordlist (inputlist, outputlist);
    if (binformat)
    {
        WriteBinaryRNNLM(modelname);
    }
    else
    {
        WriteTextRNNLM(modelname);
#if 1
        string modelfile = modelname+".oldformat";
        WriteRNNLM_oldformat(modelfile.c_str());
#endif
    }
}

void RNNLM::WriteBinaryRNNLM (string modelname)
{
    int i, a, b;
    float v;
    cpyRnnLMfromGPU ();
    FILE *fptr = NULL;
    // write to RNNLM model
    fptr = fopen (modelname.c_str(), "wb");
    if (fptr == NULL)
    {
        printf ("ERROR: Failed to create RNNLM model file(%s)\n", modelname.c_str());
        exit (0);
    }
    fwrite (&version, sizeof(float), 1, fptr);
    fwrite (&iter, sizeof(int), 1, fptr);
    fwrite (&num_layer, sizeof(int), 1, fptr);
    for (i=0; i<layersizes.size(); i++)
    {
        a = layersizes[i];
        fwrite (&a, sizeof(int), 1, fptr);
    }
    fwrite (&dim_fea, sizeof(int), 1, fptr);
    fwrite (&nclass, sizeof(int), 1, fptr);
    fwrite (&independent, sizeof(int), 1, fptr);
    fwrite (&traincritmode, sizeof(int), 1, fptr);

    for (i=0; i<num_layer; i++)
    {
        for (a=0; a<layersizes[i]; a++)
        {
            for (b=0; b<layersizes[i+1]; b++)
            {
                v = layers[i]->fetchhostvalue(a, b);
                fwrite (&v, sizeof(float), 1, fptr);
            }
        }
    }
    for (a=0; a<layersizes[1]; a++)
    {
        for (b=0; b<layersizes[1]; b++)
        {
            v = layer0_hist->fetchhostvalue(a,b);
            fwrite (&v, sizeof(float), 1, fptr);
        }
    }
    if (dim_fea > 0)
    {
        for (a=0; a<dim_fea; a++)
        {
            for (b=0; b<layersizes[1]; b++)
            {
                v = layer0_fea->fetchhostvalue(a,b);
                fwrite (&v, sizeof(float), 1, fptr);
            }
        }
    }
    if (nclass > 0)
    {
        for (a=0; a<layersizes[num_layer-1]; a++)
        {
            for (b=0; b<nclass; b++)
            {
                v = layerN_class->fetchhostvalue(a, b);
                fwrite (&v, sizeof(float), 1, fptr);
            }
        }
    }
    for (a=0; a<layersizes[1]; a++)
    {
        if (independent)    v = RESETVALUE;
        else v = neu_ac[1]->fetchhostvalue(a, 0);
        fwrite (&v, sizeof(float), 1, fptr);
    }

    a = CHECKNUM;
    fwrite (&a, sizeof(int), 1, fptr);
    fclose (fptr);
}

void RNNLM::WriteTextRNNLM (string modelname)
{
    int i, a, b;
    float v;
    cpyRnnLMfromGPU ();
    FILE *fptr = NULL;
    // write to RNNLM model
    fptr = fopen (modelname.c_str(), "w");
    if (fptr == NULL)
    {
        printf ("ERROR: Failed to create RNNLM model file(%s)\n", modelname.c_str());
        exit (0);
    }
    fprintf (fptr, "cuedrnnlm v%.1f\n", version);
    fprintf (fptr, "train file: %s\n", trainfile.c_str());
    fprintf (fptr, "valid file: %s\n", validfile.c_str());
    fprintf (fptr, "number of iteration: %d\n", iter);
    fprintf (fptr, "#train words: %d\n", trainwordcnt);
    fprintf (fptr, "#valid words: %d\n", validwordcnt);
    fprintf (fptr, "#layer: %d\n", num_layer);
    for (i=0; i<layersizes.size(); i++)
    {
        fprintf (fptr, "\tlayer %d size: %d\n", i, layersizes[i]);
    }
    fprintf (fptr, "feature dimension: %d\n", dim_fea);
    fprintf (fptr, "class layer dimension: %d\n", nclass);
    fprintf (fptr, "independent mode: %d\n", independent);
    fprintf (fptr, "train crit mode: %d\n", traincritmode);

    for (i=0; i<num_layer; i++)
    {
        fprintf (fptr, "layer %d -> %d\n", i, i+1);
        for (a=0; a<layersizes[i]; a++)
        {
            for (b=0; b<layersizes[i+1]; b++)
            {
                fprintf (fptr, "%.8f  ", layers[i]->fetchhostvalue(a, b));
            }
            fprintf (fptr, "\n");
        }
    }
    fprintf (fptr, "recurrent layer 1 -> 1\n");
    for (a=0; a<layersizes[1]; a++)
    {
        for (b=0; b<layersizes[1]; b++)
        {
            fprintf (fptr, "%.8f  ", layer0_hist->fetchhostvalue(a,b));
        }
        fprintf(fptr, "\n");
    }
    if (dim_fea > 0)
    {
        fprintf (fptr, "feature layer weight\n");
        for (a=0; a<dim_fea; a++)
        {
            for (b=0; b<layersizes[1]; b++)
            {
                fprintf (fptr, "%.8f  ", layer0_fea->fetchhostvalue(a,b));
            }
            fprintf (fptr, "\n");
        }
    }
    if (nclass > 0)
    {
        fprintf (fptr, "class layer weight\n");
        for (a=0; a<layersizes[num_layer-1]; a++)
        {
            for (b=0; b<nclass; b++)
            {
                fprintf (fptr, "%.8f  ", layerN_class->fetchhostvalue(a, b));
            }
            fprintf (fptr, "\n");
        }
    }
    fprintf (fptr, "hidden layer ac\n");
    for (a=0; a<layersizes[1]; a++)
    {
        if (independent)
        {
            fprintf (fptr, "%.8f  ", RESETVALUE);
        }
        else
        {
            fprintf (fptr, "%.8f  ", neu_ac[1]->fetchhostvalue(a, 0));
        }
    }
    fprintf(fptr, "\n");

    a = CHECKNUM;
    fprintf (fptr, "%d", a);
    fclose (fptr);
}

void RNNLM::cpyRnnLMfromGPU ()
{
    int i;
    for (i=0; i<num_layer; i++)
    {
#ifdef INPUTEMBEDDINGCPU
        if (i==0)       continue;
#endif
        // for NCE training, the weight is in CPU side
        if (i!=num_layer-1 ||  traincritmode!=2)
        {
            layers[i]->fetch();
        }
    }
    layer0_hist->fetch();
    neu_ac[1]->fetch();
    if (dim_fea > 0)
    {
        layer0_fea->fetch();
    }
    if (nclass > 0)
    {
        layerN_class->fetch();
    }
}

void RNNLM::cpyRnnLMtoGPU ()
{
    int i;
    for (i=0; i<num_layer; i++)
    {
        layers[i]->assign();
    }
    layer0_hist->assign();
    if (dim_fea > 0)
    {
        layer0_fea->assign();
    }
    if (nclass > 0)
    {
        layerN_class->assign();
    }
}

// read intput and output word list
void RNNLM::ReadWordlist (string inputlist, string outputlist)
{
    //index 0 for <s> and </s> in input and output layer
    //last node for <OOS>
    int i, a, b;
    float v;
    char word[1024];
    FILE *finlst, *foutlst;
    finlst = fopen (inputlist.c_str(), "r");
    foutlst = fopen (outputlist.c_str(), "r");
    if (finlst == NULL || foutlst == NULL)
    {
        printf ("ERROR: Failed to open input (%s) or output list file(%s)\n", inputlist.c_str(), outputlist.c_str());
        exit (0);
    }
    inputmap.insert(make_pair(string("<s>"), 0));
    outputmap.insert(make_pair(string("</s>"), 0));
    inputvec.clear();
    outputvec.clear();
    inputvec.push_back("<s>");
    outputvec.push_back("</s>");
    int index = 1;
    while (!feof(finlst))
    {
        if(fscanf (finlst, "%d%s", &i, word) == 2)
        {
            if (inputmap.find(word) == inputmap.end())
            {
                inputmap[word] = index;
                inputvec.push_back(word);
                index ++;
            }
        }
    }
    if (inputmap.find("<OOS>") == inputmap.end())
    {
        inputmap.insert(make_pair(string("<OOS>"), index));
        inputvec.push_back("<OOS>");
    }
    else
    {
        assert (inputmap["<OOS>"] == inputvec.size()-1);
    }

    index = 1;
    // allocate memory for class information
    if (nclass > 0)
    {
        word2class = new int[layersizes[num_layer]];
        classinfo = new int[nclass*3];
        classinfo[0] = 0;
    }
    int clsid, prevclsid = 0;
    while (!feof(foutlst))
    {
        if (nclass > 0)
        {
            if (fscanf(foutlst, "%d%s%d", &i, word, &clsid) == 3)
            {
                if (outputmap.find(word) == outputmap.end())
                {
                    outputmap[word] = index;
                    outputvec.push_back(word);
                    index ++;
                }
                int idx = outputmap[word];
                word2class[idx] = clsid;
                if (clsid != prevclsid)
                {
                    classinfo[prevclsid*3+1] = idx-1;
                    classinfo[prevclsid*3+2] = idx-classinfo[prevclsid*3];
                    classinfo[3*clsid]=idx;
                }
                prevclsid = clsid;
            }
        }
        else
        {
            if (fscanf(foutlst, "%d%s", &i, word) == 2)
            {
                if (outputmap.find(word) == outputmap.end())
                {
                    outputmap[word] = index;
                    outputvec.push_back(word);
                    index ++;
                }
            }
        }
    }
    if (nclass > 0)
    {
        classinfo[prevclsid*3+1] = layersizes[num_layer]-1;
        classinfo[prevclsid*3+2] = layersizes[num_layer]-classinfo[prevclsid*3];
    }
    if (outputmap.find("<OOS>") == outputmap.end())
    {
        outputmap.insert(make_pair(string("<OOS>"), index));
        outputvec.push_back("<OOS>");
    }
    else
    {
        assert (outputmap["<OOS>"] == outputvec.size()-1);
    }
    assert (inputvec.size() == layersizes[0]);
    assert (outputvec.size() == layersizes[num_layer]);
    inStartindex = 0;
    outEndindex  = 0;
    inOOSindex   = inputvec.size() - 1;
    outOOSindex  = outputvec.size() - 1;
    assert (outOOSindex == outputmap["<OOS>"]);
    assert (inOOSindex == inputmap["<OOS>"]);
    fclose (finlst);
    fclose (foutlst);
}
// write to intput and output word list
void RNNLM::WriteWordlist (string inputlist, string outputlist)
{
    int i, a, b;
    float v;
    FILE *finlst, *foutlst;
    finlst = fopen (inputlist.c_str(), "w");
    foutlst = fopen (outputlist.c_str(), "w");
    if (finlst == NULL || foutlst == NULL)
    {
        printf ("ERROR: Failed to create input (%s) or output list file(%s)\n", inputlist.c_str(), outputlist.c_str());
        exit (0);
    }
    for (i=0; i<inputvec.size(); i++)
    {
        fprintf (finlst, "%d\t%s\n", i, inputvec[i].c_str());
    }
    if (nclass > 0)
    {
        for (i=0; i<outputvec.size(); i++)
        {
            fprintf (foutlst, "%d\t%s\t%d\n", i, outputvec[i].c_str(), word2class[i]);
        }
    }
    else
    {
        for (i=0; i<outputvec.size(); i++)
        {
            fprintf (foutlst, "%d\t%s\n", i, outputvec[i].c_str());
        }
    }
    assert (inputvec.size() == layersizes[0]);
    assert (outputvec.size() == layersizes[num_layer]);
    fclose (finlst);
    fclose (foutlst);
}

void RNNLM::setTrainCritmode (string traincrit)
{
    if (traincrit == "ce")          traincritmode = 0;
    else if (traincrit == "vr")     traincritmode = 1;
    else if (traincrit == "nce")    traincritmode = 2;
    else    {printf ("ERROR: unrecognized train criterion mode: %s!\n", traincrit.c_str()); exit(0);}
    if (traincritmode == 2)
    {
        // init ncesample and dev_ncesample
        ncesample = (int *) calloc ((k+1)*minibatch, sizeof(int));
        N = (k+1)*minibatch;
        dev_ncesample = (int *) cucalloc((k+1)*minibatch*sizeof(int));
        logwordnoise = (float *) calloc (outputmap.size(), sizeof(float));
        dev_logwordnoise = (float *) cucalloc (outputmap.size()*sizeof(float));
        log_num_noise = log(k);

    }
}

void RNNLM::setLRtunemode (string lrtune)
{
    if (lrtune == "newbob")         lrtunemode = 0;
    else if (lrtune == "adagrad")   lrtunemode = 1;
    else if (lrtune == "rmsprop")   lrtunemode = 2;
    else        {printf ("ERROR: unrecognized learn rate tune mode: %s!\n", lrtune.c_str()); exit(0);}
    if (lrtunemode != 0)
    {
        gradlayers.resize(num_layer);
        accgradlayers.resize(num_layer);
        for (int i=0; i<num_layer; i++)
        {
            int nrow = layersizes[i];
            int ncol = layersizes[i+1];
            gradlayers[i] = new matrix (nrow, ncol);
            accgradlayers[i] = new matrix (nrow, ncol);
            gradlayers[i]->initmatrix();
            accgradlayers[i]->initmatrix();
        }
        gradlayer0_hist = new matrix (layersizes[1], layersizes[1]);
        accgradlayer0_hist = new matrix (layersizes[1], layersizes[1]);
        gradlayer0_hist->initmatrix();
        accgradlayer0_hist->initmatrix();
    }
}

void RNNLM::setIndependentmode (int v)
{
    independent = v;
    if (independent)
    {
        bptt_delay = 1;  //for independent mode, bptt_delay alwasy be 1.
    }
}

void RNNLM::prepareNCEtrain ()
{
    int i;
    for (i=0; i<outputmap.size(); i++)
    {
        unigram[i] = unigram[i];
        if (unigram[i] == 0)
        {
            printf ("ERROR: word: %s has not been observed in train data\n", inputvec[i].c_str());
            // exit (0);
            break;
        }
    }
    // make sure each word has a probability to be sampled
    // if (i!= outputmap.size())
#if 0
    {
        float intpltwgt = 0.0;
        for (i=0; i<outputmap.size(); i++)
        {
            unigram[i] = (1-intpltwgt)*unigram[i] + intpltwgt/outputmap.size();
            if (i==0)   accprob[i] = unigram[i];
            else        accprob[i] = unigram[i] + accprob[i-1];
        }
    }
#endif
    for (i=0; i<outputmap.size(); i++)
    {
        unigram[i] = unigram[i]*exp(lognormconst);
    }
    for (i=0; i<outputmap.size(); i++)
    {
        logwordnoise[i] = log_num_noise + logunigram[i] + lognormconst;
    }
}

int RNNLM::genOnesample ()
{
    real sprob = randomv(0, 1);
    int outputlayersize = outputmap.size();
    int downbound = 0;
    int upbound  = outputlayersize + 1;
    int mid = (upbound+downbound)/2;
    if (accprob[0] > sprob)
    {
        return 0;
    }
    else if (accprob[outputlayersize-1] < sprob)
    {
        return outputlayersize - 1;
    }
    else
    {
        while (1)
        {
            if (accprob[mid]<=sprob && accprob[mid+1]>=sprob)
            {
                break;
            }
            if (accprob[mid] < sprob)
            {
                downbound = mid;
                mid = (upbound + downbound)/2;
            }
            else
            {
                upbound = mid;
                mid =  (upbound + downbound)/2;
            }
        }
        return mid+1;
    }
}

void RNNLM::genNCESample ()
{
    // #pragma omp parallel for num_threads (1)
    for (int i=0; i<N; i++)
    {
        int sampleid;
        if (i % (k+1) == 0)
        {
            int mbindex = i/(k+1);
            sampleid = host_curwords[mbindex];
        }
        else
        {
            sampleid = genOnesample ();
        }
        ncesample[i] = sampleid;
    }
}

void RNNLM::forward_NCE ()
{
    //TODO: if too slow, speed it up!
    // layers[num_layer-1]->fetch();
    neu_ac[num_layer-1]->fetch();
    // int a, n;
    //TODO: parallel it using multi thread
#ifdef NUM_THREAD
#pragma omp parallel for num_threads(NUM_THREAD)
#else
#pragma omp parallel for num_threads(nthread)
#endif
    for (int n=0; n<N; n++)
    {
        int sample = ncesample[n];
        int mbindex = n/(k+1);
        if (noupdateset.find(mbindex) == noupdateset.end())
        {
            float v = 0;
            for (int a=0; a<layersizes[num_layer-1]; a++)
            {
                float ac = neu_ac[num_layer-1]->fetchhostvalue(a, mbindex);
                float weight = layers[num_layer-1]->fetchhostvalue(a, sample);
                v += ac*weight;
            }
#ifndef LOGADD
            v = exp(v);
#endif
            neu_ac[num_layer]->assignhostvalue(sample, mbindex, v);
        }
    }
    //TODO: not necessary actually
}

void RNNLM::calerr_NCE()
{
#ifdef NUM_THREAD
#pragma omp parallel for num_threads(NUM_THREAD)
#else
#pragma omp parallel for num_threads(nthread)
#endif
    for (int n=0; n<N; n++)
    {
        int sample = ncesample[n];
        int mbindex = n/(k+1);
        if (noupdateset.find(mbindex) == noupdateset.end())
        {
            float ac = neu_ac[num_layer]->fetchhostvalue(sample, mbindex);
#ifdef LOGADD
            float er = - exp (ac - logadd(ac, logwordnoise[sample]));
#else
            float er = - ac/(ac+k*unigram[sample]);
#endif
            neu_er[num_layer]->assignhostvalue(sample, mbindex, er);
        }
    }
}

void RNNLM::bpttAndupdate_NCE ()
{
    int nrow = layersizes[num_layer-1];
    int ncol = layersizes[num_layer];
    neu_er[num_layer-1]->fetch();
    // back prop
#ifdef NUM_THREAD
#pragma omp parallel for num_threads(NUM_THREAD)
#else
#pragma omp parallel for num_threads(nthread)
#endif
    for (int n=0; n<N; n++)
    {
        int sample = ncesample[n];
        int mbindex = n/(k+1);
        if (noupdateset.find(mbindex) == noupdateset.end())
        {
            float er = neu_er[num_layer]->fetchhostvalue(sample, mbindex);
            if (n % (k+1) == 0)
                er = 1 + er;
            float v = 0;
            for (int a=0; a<nrow; a++)
            {
                float weight = layers[num_layer-1]->fetchhostvalue(a, sample);
                v = er * weight;
                neu_er[num_layer-1]->addhostvalue(a, mbindex, v);
            }
        }
    }
    // update weight
#ifdef NUM_THREAD
#pragma omp parallel for num_threads(NUM_THREAD)
#else
#pragma omp parallel for num_threads(nthread)
#endif
#if 0
    // this is not right to use multi threads
    // it will conflict when more than one same words
    for (int n=0; n<N; n++)
    {
        int sample = ncesample[n];
        int mbindex = n/(k+1);
        if (noupdateset.find(mbindex) == noupdateset.end())
        {
            float er = neu_er[num_layer]->fetchhostvalue(sample, mbindex);
            if (n % (k+1) == 0)
                er = 1+er;
            float v = 0;
            for (int a=0; a<nrow; a++)
            {
                float ac = neu_ac[num_layer-1]->fetchhostvalue(a, mbindex);
                v = alpha * er * ac;
                layers[num_layer-1]->addhostvalue(a, sample, v);
            }
        }
    }
#else
#ifdef MBUPDATEOUTPUTLAYER
    for (int a=0; a<nrow; a++)
    {
        for (int n=0; n<N; n++)
        {
            int sample = ncesample[n];
            int mbindex = n/(k+1);
            if (noupdateset.find(mbindex) == noupdateset.end())
            {
                float er = neu_er[num_layer]->fetchhostvalue(sample, mbindex);
                float ac = neu_ac[num_layer-1]->fetchhostvalue(a, mbindex);
                if (n % (k+1) == 0)
                    er = 1+er;
                float v = 0;
                if (lrtunemode == 0)
                {
                    v = alpha * er * ac;
                    layers[num_layer-1]->addhostvalue(a, sample, v);
                }
                else
                {
                    v = er*ac;
                    gradlayers[num_layer-1]->addhostvalue(a, sample, v);
                }
            }
        }

        if (lrtunemode != 0)
        {
            for (int n=0; n<N; n++)
            {
                int sample = ncesample[n];
                int mbindex = n/(k+1);
                if (noupdateset.find(mbindex) == noupdateset.end())
                {
                    float v = gradlayers[num_layer-1]->fetchhostvalue(a, sample);
#ifdef RMSPROP
                    if (v==0) continue;
                    float acc = accgradlayers[num_layer-1]->fetchhostvalue(a, sample);
                    if (acc == 0)
                    {
                        acc = v*v;
                    }
                    else
                    {
                        acc = RMSPROPCOFF*acc + (1-RMSPROPCOFF)*v*v;
                    }
                    accgradlayers[num_layer-1]->assignhostvalue(a, sample, acc);
#else
                    accgradlayers[num_layer-1]->addhostvalue(a, sample, v*v);
#endif
                    v = alpha*v/sqrt(FUDGE_FACTOR+accgradlayers[num_layer-1]->fetchhostvalue(a, sample));
                    layers[num_layer-1]->addhostvalue(a, sample, v);
                    gradlayers[num_layer-1]->assignhostvalue(a, sample, 0);
                }
            }
        }
    }
#else
    for (int a=0; a<nrow; a++)
    {
        for (int n=0; n<N; n++)
        {
            int sample = ncesample[n];
            int mbindex = n/(k+1);
            if (noupdateset.find(mbindex) == noupdateset.end())
            {
                float er = neu_er[num_layer]->fetchhostvalue(sample, mbindex);
                float ac = neu_ac[num_layer-1]->fetchhostvalue(a, mbindex);
                if (n % (k+1) == 0)
                    er = 1+er;
                float v = 0;
                if (lrtunemode == 0)
                {
                    v = alpha * er * ac;
                }
                else
                {
                    v = er*ac;
#ifdef RMSPROP
                    float acc = accgradlayers[num_layer-1]->fetchhostvalue(a, sample);
                    if (acc == 0)
                    {
                        acc = v*v;
                    }
                    else
                    {
                        acc = RMSPROPCOFF*acc + (1-RMSPROPCOFF)*v*v;
                    }
                    accgradlayers[num_layer-1]->assignhostvalue(a, sample, acc);
#else
                    accgradlayers[num_layer-1]->addhostvalue(a, sample, v*v);
#endif
                    v = alpha*v/sqrt(FUDGE_FACTOR+accgradlayers[num_layer-1]->fetchhostvalue(a, sample));
                }
                layers[num_layer-1]->addhostvalue(a, sample, v);
            }

        }
    }
#endif
#endif

    // TODO: speed it up if necessary
    // layers[num_layer-1]->assign();
    neu_er[num_layer-1]->assign();
}

void RNNLM::updateInputWordWgts(int *inwords)
{
    neu_er[1]->fetch();
#ifdef NUM_THREAD
#pragma omp parallel for num_threads(NUM_THREAD)
#else
#pragma omp parallel for num_threads(nthread)
#endif
    for (int j=0; j<layersizes[1]; j++)
    {
        float er, word;
        for (int mb=0; mb<minibatch; mb++)
        {
            // word = host_prevwords[mb];
            word = inwords[mb];
            if (lrtunemode == 0)
            {
                er = alpha*neu_er[1]->fetchhostvalue(j, mb);
            }
            else        // adagrad
            {
                er = neu_er[1]->fetchhostvalue(j, mb);
#ifdef RMSPROP
                float acc = accgradlayers[0]->fetchhostvalue(word, j);
                if (acc == 0)
                {
                    acc = er*er;
                }
                else
                {
                    acc = RMSPROPCOFF*acc + (1-RMSPROPCOFF)*er*er;
                }
                accgradlayers[0]->assignhostvalue(word, j, acc);
#else
                accgradlayers[0]->addhostvalue(word, j, er*er);
#endif
                // gradlayers[0]->addhostvalue(word, j, er);
                er = alpha*er/sqrt (1e-8+accgradlayers[0]->fetchhostvalue(word, j));
            }
            layers[0]->addhostvalue(word, j, er);
        }
    }
}

void RNNLM::forwardInputWordWgts ()
{
#ifdef NUM_THREAD
#pragma omp parallel for num_threads(NUM_THREAD)
#else
#pragma omp parallel for num_threads(nthread)
#endif
    for (int mb=0; mb<minibatch; mb++)
    {
        int word = host_prevwords[mb];
        for (int j=0; j<layersizes[1]; j++)
        {
            float v = layers[0]->fetchhostvalue(word, j);
            neu_ac[1]->assignhostvalue(j, mb, v);
        }
    }
    neu_ac[1]->assign();
}

void RNNLM::copyRecurrentAc ()
{
    float *srcac, *dstac;
    assert (minibatch == 1);
    srcac = neu_ac[1]->gethostdataptr();
    dstac = neu0_ac_hist->gethostdataptr();
    memcpy (dstac, srcac, sizeof(float)*layersizes[1]);
}

void RNNLM::ResetRechist()
{
    int a = 0;
    for (int i=0; i<layersizes[1]; i++)
    {
        neu0_ac_hist->assignhostvalue(i, 0, RESETVALUE);
        resetAc[i] = RESETVALUE;
    }
#if 0
    float *ac = neu0_ac_hist->gethostdataptr();
    memcpy (ac, resetAc, sizeof(float)*layersizes[1]);
#endif
}

float RNNLM::forword (int prevword, int curword)
{
   int a, b, c, nrow, ncol;
   float v, norm, maxv;
   nrow = layersizes[1];
   ncol = layersizes[1];
   float *srcac, *wgt, *dstac;

   // neu0 -> neu1
   for (a=0; a<layers.size(); a++)
   {
       if (a==0)
       {
           srcac = neu0_ac_hist->gethostdataptr();
           wgt   = layer0_hist->gethostdataptr();
           dstac = neu_ac[1]->gethostdataptr();
           nrow  = layersizes[1];
           ncol  = layersizes[1];
           memset(dstac, 0, sizeof(float)*ncol);
           for (b=0; b<ncol; b++)
           {
               dstac[b] = layers[0]->fetchhostvalue(prevword, b);
           }
           if (dim_fea > 0)
           {
               float *feasrcac = neu0_ac_fea->gethostdataptr();
               float *feawgt   = layer0_fea->gethostdataptr();
               matrixXvector (feasrcac, feawgt, dstac, dim_fea, ncol);
           }
       }
       else
       {
           srcac = neu_ac[a]->gethostdataptr();
           wgt   = layers[a]->gethostdataptr();
           dstac = neu_ac[a+1]->gethostdataptr();
           nrow  = layersizes[a];
           ncol  = layersizes[a+1];
           memset(dstac, 0, sizeof(float)*ncol);
       }
       if (a+1==num_layer)
       {
           if (lognormconst < 0)
           {
               if (nclass > 0)
               {
                   float *clsdstac = neuN_ac_class->gethostdataptr();
                   float *clswgt = layerN_class->gethostdataptr();
                   int ncol_cls = nclass;
                   matrixXvector (srcac, clswgt, clsdstac, nrow, ncol_cls);
                   neuN_ac_class->hostsoftmax();
                   int clsid = word2class[curword];
                   int swordid = classinfo[clsid*3];
                   int ewordid = classinfo[clsid*3+1];
                   int nword   = classinfo[clsid*3+2];
                   matrixXvector(srcac, wgt+swordid*nrow, dstac+swordid, nrow, nword);
                   neu_ac[a+1]->hostpartsoftmax(swordid, ewordid);
               }
               else
               {
                   matrixXvector (srcac, wgt, dstac, nrow, ncol);
                   neu_ac[a+1]->hostsoftmax();
               }
           }
           else
           {
               float v = 0;
               for (int i=0; i<nrow; i++)
               {
                   // v += srcac[i]*wgt[i+curword*nrow];
                   v += neu_ac[a]->fetchhostvalue(i, 0)*layers[a]->fetchhostvalue(i, curword);
               }
               dstac[curword] = exp(v-lognormconst);
           }
       }
       else
       {
           matrixXvector (srcac, wgt, dstac, nrow, ncol);
           neu_ac[a+1]->hostsigmoid();
       }
   }
   if (nclass > 0)
   {
       int clsid = word2class[curword];
       return neu_ac[num_layer]->fetchhostvalue(curword, 0) * neuN_ac_class->fetchhostvalue(clsid, 0);
   }
   else
   {
       return neu_ac[num_layer]->fetchhostvalue(curword, 0);
   }
}

void RNNLM::matrixXvector (float *src, float *wgt, float *dst, int nr, int nc)
{
    int i, j;
#if 1
#ifdef NUM_THREAD
#pragma omp parallel for num_threads(NUM_THREAD)
#else
#pragma omp parallel for num_threads(nthread)
#endif
#endif
    for (i=0; i<nc; i++)
    {
        for (j=0; j<nr; j++)
        {
            dst[i] += src[j]*wgt[j+i*nr];
        }
    }
    return;
}

void RNNLM::ReadFeaFile(string filestr)
{
    int i, j, t;
    float value;
    FILE *fptr = fopen (filestr.c_str(), "r");
    if (fptr == NULL)
    {
        printf ("Error: Failed to open feature file: %s\n", filestr.c_str());
        exit(0);
    }
    fscanf (fptr, "%d %d", &num_fea, &dim_fea);
    // if the fea file is two large, just allocate cpu memory
    feamatrix = new matrix (dim_fea, num_fea);
    feamatrix->initmatrix();
    if (debug > 1)
    {
        printf ("%d lines feature (with %d dimensions) will be read from %s\n", num_fea, dim_fea, filestr.c_str());
    }
    i = 0;
    while (i < num_fea)
    {
        if (feof(fptr))         break;
        fscanf (fptr, "%d", &j);
        assert (j == i);
        for (t=0; t<dim_fea; t++)
        {
            fscanf (fptr, "%f", &value);
            feamatrix->assignhostvalue(t, i, value);
        }
        i ++;
    }
    if (i != num_fea)
    {
        printf ("Warning: only read %d lines from the feature file: %s, should be %d lines\n", i, filestr.c_str(), num_fea);
    }
    feamatrix->assign();
    if (debug > 1)
    {
        printf ("%d feature lines (with %d dimensions) is read from %s successfully\n", num_fea, dim_fea,  filestr.c_str());
    }
    fclose(fptr);

    // allocate memeory for additional feature in input layer
    // if memory already allocated during laoding model
    if (layer0_fea == NULL)
    {
        layer0_fea = new matrix(dim_fea, layersizes[1]);
        layer0_fea->random (MINRANDINITVALUE, MAXRANDINITVALUE);
        neu0_ac_fea = new matrix (dim_fea, minibatch);
        neu0_ac_fea->initmatrix();
    }
}

void RNNLM::fillInputFeature (matrix *neu0_ac_fea, matrix *feamatrix, int *feaindices, int *mbfeaindices)
{
    int i, t, index;
    for (i=0; i<minibatch; i++)
    {
        t = mbfeaindices[i];
        index = feaindices[t];
        cucpyInGpu (neu0_ac_fea->getdevdataptr(0, i), feamatrix->getdevdataptr(0, index), sizeof(float)*dim_fea);
    }
}

void RNNLM::setNclass (int n)
{
    nclass = n;
    int nlayer = layersizes.size();
    if (nclass>0 && layerN_class==NULL)
    {
        layerN_class = new matrix (layersizes[nlayer-2], nclass);
        layerN_class->random(MINRANDINITVALUE, MAXRANDINITVALUE);
        neuN_ac_class = new matrix (nclass, minibatch);
        neuN_er_class = new matrix (nclass, minibatch);
        neuN_ac_class->initmatrix();
        neuN_er_class->initmatrix();
    }
}

void RNNLM::getClassLabels ()
{
    for (int i=0; i<minibatch; i++)
    {
        if (host_curwords[i] == INVALID_INT)
        {
            host_curclass[i] = 0;
        }
        else
        {
            host_curclass[i] = word2class[host_curwords[i]];
        }
    }
    cucpytoGpu (dev_curclass, host_curclass, sizeof(int)*minibatch);
}

void RNNLM::clusterWord2Class (string trainfile, map<string, int> &wordmap, vector<string> &wordvec)
{
    if (word2class == NULL)
    {
        // each word assign a class
        // now assume each class has the same number of word
        int size=wordvec.size(), i, clsid, prevclsid;
        word2class = new int[size];
        classinfo = new int[nclass*3];
        for (i=0; i<size; i++)
        {
            if (i==0)       word2class[i] = 0;
            else if (i==size-1)  word2class[i] = nclass-1;
            else
            {
                clsid = i/((size-2)/(nclass-2)+1)+1;
                word2class[i] = clsid;
            }
        }
        prevclsid = 0;
        classinfo[0] = 0;
        for (i=1; i<size; i++)
        {
            clsid = word2class[i];
            if (clsid != prevclsid)
            {
                classinfo[prevclsid*3+1]=i-1;
                classinfo[prevclsid*3+2]=i-classinfo[prevclsid*3];
                classinfo[3*clsid] = i;
            }
            prevclsid = clsid;
        }
        classinfo[prevclsid*3+1]=i-1;
        classinfo[prevclsid*3+2]=i-classinfo[prevclsid*3];
    }
#if 0
    for (int i=0; i<layersizes[num_layer]; i++)
    {
        printf ("word[%d]:%s\tclasid:%d\n", i, outputvec[i].c_str(), word2class[i]);
    }
    for (int i=0; i<nclass; i++)
    {
        printf ("class[%d], sword=%d, eword=%d, nword=%d\n", i, classinfo[3*i], classinfo[3*i+1], classinfo[3*i+2]);
    }
#endif
    // copy to GPU
    dev_classinfo = (int *)cucalloc(sizeof(int)*nclass*3);
    cucpytoGpu(dev_classinfo, classinfo, sizeof(int)*nclass*3);
    host_curclass = new int [minibatch];
    dev_curclass = (int *)cucalloc(sizeof(int)*minibatch);
}
